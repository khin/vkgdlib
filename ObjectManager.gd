extends Control

################################################################################

export(NodePath) var _manager

################################################################################

onready var manager = get_node(_manager)
onready var mesh_list = $ButtonAdd.get_popup()

var object_counter = 0

################################################################################

func _ready():
	mesh_list.connect("index_pressed", self, "on_create_click")

func on_create_click(index):
	var mesh
	var option = mesh_list.get_item_text(index)
	match option:
		"Capsule":  mesh = CapsuleMesh.new()
		"Cube":     mesh = CubeMesh.new()
		"Cylinder": mesh = CylinderMesh.new()
		"Plane":    mesh = PlaneMesh.new()
		"Prism":    mesh = PrismMesh.new()
		"Sphere":   mesh = SphereMesh.new()
		"Load OBJ":
			if not $ButtonAdd/ObjFileDialog.is_connected("file_selected", self, "loaded_obj"):
				$ButtonAdd/ObjFileDialog.connect("file_selected", self, "loaded_obj", [], CONNECT_ONESHOT)
			$ButtonAdd/ObjFileDialog.popup_centered()
			return
		_: return
	
	var geometry_name = option.to_lower() + "_" + String(object_counter)
	add_geometry(geometry_name, mesh)

func loaded_obj(path):
	var geometry_name = path.get_file().get_basename() + "_" + String(object_counter)
	var mesh = load(path)
	add_geometry(geometry_name, mesh)

func add_geometry(geometry_name, mesh):
	if not manager.add_geometry(geometry_name, mesh):
		return
	
	var geometry = manager.get_geometry(geometry_name)
	$ObjectList.add_item(geometry_name)
	$ObjectList.set_item_metadata(object_counter, geometry)
	object_counter += 1

################################################################################
