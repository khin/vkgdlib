extends Container

################################################################################

export(PackedScene) var edit_scene
export(NodePath) var _manager

################################################################################

func parse_material_uniforms(mat):
	clear_uniforms()
	
	yield(get_tree(), "idle_frame")
	
	var uniforms = mat.get_exported_uniforms()
	for uniform in uniforms:
		add_uniform(uniform)

func clear_uniforms():
	for node in $Scroll/List.get_children():
		node.queue_free()

func add_uniform(uniform):
	var editor = edit_scene.instance()
	editor.manager = get_node(_manager)
	$Scroll/List.add_child(editor)
	editor.set_uniform(uniform)

func get_uniforms():
	var uniforms = []
	
	for editor in $Scroll/List.get_children():
		var uniform = editor.get_uniform()
		uniforms.append(uniform)
	
	return uniforms

################################################################################
