#pragma once

#include <vector>
#include <array>
#include <map>

#include <vulkan/vulkan.hpp>
#include <glm/glm.hpp>

#include <Godot.hpp>
#include <Reference.hpp>

#include "material.h"

using namespace godot;

struct UniformBufferData {
	vk::Buffer buffer;
	vk::DeviceMemory memory;
	size_t size;
};

class Geometry : public GodotScript<Reference> {
	GODOT_CLASS(Geometry)

public:
	Geometry();
	~Geometry();

	static void _register_methods();
	void setPosition(Vector3);
	void setRotation(Vector3);
	void setScale(Vector3);
	Vector3 getPosition() const;
	Vector3 getRotation() const;
	Vector3 getScale() const;
	void updateModelMatrix();

	void setArrayMesh(Object*);
	void setMaterial(Object*);
	String getMaterialName() const;

	void createBuffers();
	void createVertexBuffer();
	void createIndexBuffer();
	void createUniformBuffer();

	void createDescriptorSet();
	void updateDescriptorSets();

	void recordDrawingCommands(vk::CommandBuffer);
	void updateUniformBuffer(DefaultUniforms);

	void cleanup();

protected:
	glm::vec3 position;
	glm::vec3 rotation;
	glm::vec3 scale;

	std::vector<float> vertexData;

	std::vector<glm::vec3> vertices = {
		{ -0.5f, -0.5f, 0.0f }, {  0.5f, -0.5f, 0.0f },
		{  0.5f,  0.5f, 0.0f },	{ -0.5f,  0.5f, 0.0f }
	};

	std::vector<glm::vec3> normals = {
		{ 1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f },
		{ 0.0f, 0.0f, 1.0f }, { 1.0f, 1.0f, 1.0f }
	};

	std::vector<glm::vec4> tangents = {
		{ 1.0f, 0.0f, 0.0f, 1.0f }, { 0.0f, 1.0f, 0.0f, 1.0f },
		{ 0.0f, 0.0f, 1.0f, 1.0f }, { 1.0f, 1.0f, 1.0f, 1.0f }
	};

	std::vector<glm::vec2> uvs = {
		{ 1.0f, 0.0f }, { 0.0f, 0.0f },
		{ 0.0f, 1.0f }, { 1.0f, 1.0f }
	};

	std::vector<uint32_t> indices = {
		4, 5, 6, 6, 7, 4,
		0, 1, 2, 2, 3, 0
	};

	glm::mat4 modelMatrix;

	Material *material;

	vk::Buffer vertexBuffer;
	vk::Buffer indexBuffer;
	bool indexed;

	vk::DeviceMemory vertexBufferMem;
	vk::DeviceMemory indexBufferMem;

	std::map<uint32_t, UniformBufferData> uniformBuffers;

	vk::DescriptorSet descriptorSet;
	vk::DescriptorPool poolFrom;
};
