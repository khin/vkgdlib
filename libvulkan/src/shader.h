#pragma once

#include <vector>
#include <string>

#include <Godot.hpp>
#include <Reference.hpp>

#include <vulkan/vulkan.hpp>
#include <shaderc/shaderc.hpp>

using namespace godot;

class Shader : public GodotScript<Reference> {
	GODOT_CLASS(Shader)

public:
	enum Type : int {
		UNDEFINED = -1,
		VERTEX    = 0,
		FRAGMENT  = 1
	};

	~Shader();
	Shader();

	static void _register_methods();

	void setName(String);
	String getName() const;
	void gdSetType(int type);
	int gdGetType() const;

	void setType(Type);
	Type getType() const;
	std::string getTypeString() const;

	void loadSourceFile(String source_filename);
	void reloadSourceFile();
	std::string getFilename() const;
	String gdGetFilename() const;
	bool compile();

	String getCompilationStatusString() const;
	const std::vector<uint32_t>& getCode() const;
	//void saveSpirvFile(String filename) const;

	void createModule();
	void destroyModule();
	vk::ShaderModule getModule() const;

	static std::string typeString(Type);
	static vk::ShaderStageFlagBits typeVulkan(Type);
	static shaderc_shader_kind typeShaderC(Type);
	static Type deduceType(std::string filename);

protected:
	String name;

	Type type;
	std::string filename;
	std::string source;
	std::string errorString;
	std::vector<uint32_t> code;

	vk::ShaderModule module;
};
