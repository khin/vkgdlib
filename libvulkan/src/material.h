#pragma once

#include <array>
#include <vector>
#include <string>
#include <map>

#include <vulkan/vulkan.hpp>
#include <glm/glm.hpp>

#include <Godot.hpp>
#include <Reference.hpp>

#include "shader.h"
#include "texture.h"

using namespace godot;

struct DefaultUniforms {
	float delta;

	// mat4 model;
	glm::mat4 view;
	glm::mat4 projection;

	// mat4 modelView;
	// mat4 modelViewProjection

	glm::mat4 cameraMatrix; // viewProjection
	//glm::mat3 normalMatrix; // glm::inverseTranspose(glm::mat3(modelView))

	glm::mat4 viewInverse;
	glm::mat4 projectionInverse;
};

struct UBOMember {
	enum Type {
		UNKNOWN = 0,

		INT,
		UINT,
		FLOAT,

		IVEC2,
		IVEC3,

		UVEC2,
		UVEC3,

		VEC2,
		VEC3,
		COLOR,	// 'vec4'

		MAT3,
		MAT4
	};

	std::string name = "";
	Type type = Type::UNKNOWN;

	uint32_t size     = 0;
	uint32_t offset   = 0;
	uint32_t uboIndex = 0;

	std::string toString() const;
	static std::string type2string(Type);
};

struct UniformBuffer {
	std::string name = "";

	uint32_t binding = 0;
	uint32_t size    = 0;
	uint32_t offset  = 0;
	std::vector<UBOMember> members;

	std::string toString() const;
};

struct ImageSampler {
	enum Type {
		UNKNOWN = 0,

		SAMPLER2D,
		CUBEMAP
	};

	std::string name = "";
	Type type = Type::UNKNOWN;

	uint32_t binding  = 0;

	Texture texture;

	std::string toString() const;
	static std::string type2string(Type);
};

struct VertexAttribute {
	enum Type {
		UNKNOWN = 0,

		INT,
		UINT,
		FLOAT,

		IVEC2,
		IVEC3,

		UVEC2,
		UVEC3,

		VEC2,
		VEC3,
		VEC4
	};

	std::string name;
	Type type = Type::UNKNOWN;

	vk::Format format;
	uint32_t size = 0;
	uint32_t offset = 0;
	uint32_t location = 0;

	std::string toString() const;
	static std::string type2string(Type);
};

class Material : public GodotScript<Reference> {
	GODOT_CLASS(Material)

public:
	~Material();
	Material();

	static void _register_methods();

	void setDefaultTextureImage(Object *image);
	const godot::Image* getDefaultTextureImage() const;

	void setShader(Object*);
	bool hasShader(int type) const;
	void setName(String);
	String getName() const;
	String getShaderName(int type) const;
	Array getExportedUniforms() const;
	void setExportedUniformsValues(Array);

	void setup(String material_name);
	void parseShaders();
	void parseExportUniforms();
	void createDescriptorSetLayout();
	void createDescriptorPool();
	void createPipeline();

	vk::Pipeline getPipeline();
	vk::PipelineLayout getPipelineLayout();
	vk::DescriptorPool getDescriptorPool();
	vk::DescriptorSetLayout getDescriptorSetLayout();

	void cleanup();
	void printDebug() const;

	vk::VertexInputBindingDescription getBindingDescription() const;
	std::vector<vk::VertexInputAttributeDescription> getAttributeDescriptions() const;

	const std::vector<ImageSampler>& getSamplers() const;
	const std::vector<UniformBuffer>& getUniforms() const;
	const std::vector<VertexAttribute>& getAttributes() const;
	uint32_t getAttributesStride() const;
	uint32_t getUniformBuffersSize() const;

	uint32_t getUboCount() const;
	uint32_t getSamplerCount() const;

	void setTexture(String samplerName, Object* image);

protected:
	UniformBuffer& getUniformBuffer(std::string name);
	ImageSampler&  getImageSampler(std::string name);

	String name;

	godot::Image *defaultTexture = nullptr;

	std::array<Shader*, 2>       shaders;
	std::vector<UniformBuffer>   uniforms;
	std::vector<ImageSampler>    samplers;
	std::vector<VertexAttribute> attributes;

	uint32_t attributesStride = 0;
	uint32_t uniformBuffersSize = 0;

	Array exportedUniforms;

	vk::Pipeline pipeline;
	vk::PipelineLayout pipelineLayout;

	vk::DescriptorPool descriptorPool;
	vk::DescriptorSetLayout descriptorSetLayout;
};
