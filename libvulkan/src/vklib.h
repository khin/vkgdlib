#pragma once

#include <vector>

#include <Godot.hpp>
#include <Node.hpp>

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <vulkan/vulkan.hpp>
#include <glm/glm.hpp>

#include "shader.h"
#include "material.h"
#include "geometry.h"
#include "camera.h"

using namespace godot;

const int WIDTH = 1024;
const int HEIGHT = 768;

struct VkQueueFamilyIndices {
	int graphics = -1;
	int present = -1;

	VkQueueFamilyIndices();
	VkQueueFamilyIndices(vk::PhysicalDevice, vk::SurfaceKHR);

	inline bool isComplete() {
		return graphics >= 0 && present >= 0;
	}
};

struct SwapchainSupport {
	vk::SurfaceCapabilitiesKHR capabilities;
	std::vector<vk::SurfaceFormatKHR> formats;
	std::vector<vk::PresentModeKHR> presentModes;

	SwapchainSupport();
	SwapchainSupport(vk::PhysicalDevice, vk::SurfaceKHR);
};


class VKLib : public GodotScript<Node> {
	GODOT_CLASS(VKLib)

public:
	VKLib();
	~VKLib();

	// ################################# //
	// ########## Godot stuff ########## //

	static void _register_methods();
	void _ready();
	void _process(float delta);

	void addGeometry(Object*);
	void cleanup();

	// ################################## //
	// ########## Vulkan stuff ########## //

	static VKLib* getSingleton();
	static vk::Device getDevice();
	static vk::PhysicalDevice getPhysicalDevice();

	vk::RenderPass getRenderPass() const;
	vk::Viewport getViewport() const;
	vk::Rect2D getScissor() const;

	vk::CommandBuffer beginSingleTimeCommands();
	void endSingleTimeCommands(vk::CommandBuffer);

	// #################### //

	static void mouseClickCallback(
		GLFWwindow*, int button, int action, int mods
	);

	static void mouseMotionCallback(
		GLFWwindow*, double x, double y
	);

	void input(float delta);

protected:
	GLFWwindow *window;

	const std::vector<const char*> deviceExtensions = {
		"VK_KHR_swapchain"
	};

	#ifdef DEBUG_ENABLED
		const std::vector<const char*> validationLayers = {
			"VK_LAYER_LUNARG_standard_validation"
		};

		bool checkValidationLayersSupport();

		vk::DebugReportCallbackEXT debugCallback;
		bool setupDebugCallback();
		bool destroyDebugCallback();
	#endif

	std::vector<Geometry*> geometries;

	vk::Instance instance;
	vk::SurfaceKHR surface;

	vk::PhysicalDevice physicalDevice;
	vk::Device device;
	vk::Queue qGraphics;
	vk::Queue qPresent;

	vk::SwapchainKHR swapchain;
	vk::Format format;
	vk::Extent2D extent;
	vk::Viewport viewport;
	vk::Rect2D scissor;
	std::vector<vk::Image> images;
	std::vector<vk::ImageView> imageViews;
	std::vector<vk::Framebuffer> framebuffers;
	std::vector<vk::CommandBuffer> commandBuffers;

	vk::Image depthImage;
	vk::ImageView depthImageView;
	vk::DeviceMemory depthImageMemory;

	vk::RenderPass renderpass;
	vk::CommandPool commandPool;

	vk::Semaphore semImageAvailable;
	vk::Semaphore semRenderFinished;

	void initWindow();
	void initVulkan();
		std::vector<const char*> getRequiredExtensions();
		void createVkInstance();
		void createSurface();

		bool checkDeviceExtensionsSupport(vk::PhysicalDevice);
		bool isDeviceSuitable(vk::PhysicalDevice);
		void pickPhysicalDevice();
		void createLogicalDevice();

		void createSwapchain();
		void createSwapchainImageViews();

		void createRenderPass();
		void createCommandPool();

		void createDepthResources();
		void createFramebuffers();

		void clearCommandBuffers();
		void allocateCommandBuffers();
		void createSemaphores();

		void drawFrame();

	static constexpr float cameraSpeed = 10.0f;
	static constexpr float cameraAngularSpeed = 10.0f;
	static bool cameraMoving;
	static glm::vec2 lastMousePosition;
	Camera camera;
	DefaultUniforms uniforms;

private:
	static VKLib *singleton;
};
