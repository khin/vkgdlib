#include "shader.h"

#include <Godot.hpp>
#include <fstream>
#include <streambuf> // for istreambuf_iterator in loadSourceFile

#include <utility>

#include "vklib.h"

// ##############################

using namespace godot;
using std::string;
using std::vector;

// ##############################

Shader::~Shader() {}

Shader::Shader(){
	setType(Type::UNDEFINED);
}

void Shader::_register_methods() {
	register_method("set_name", &Shader::setName);
	register_method("get_name", &Shader::getName);

	register_method("set_type", &Shader::gdSetType);
	register_method("get_type", &Shader::gdGetType);

	register_method("load", &Shader::loadSourceFile);
	register_method("reload", &Shader::reloadSourceFile);
	register_method("get_filepath", &Shader::gdGetFilename);
	register_method("compile", &Shader::compile);
	register_method("get_compilation_status_string", &Shader::getCompilationStatusString);

	register_method("create_module", &Shader::createModule);
	register_method("destroy_module", &Shader::destroyModule);
}

// ##############################

void Shader::setName(String name) {
	this->name = name;
}

String Shader::getName() const {
	return name;
}

void Shader::gdSetType(int t) {
	if (t > Type::FRAGMENT || t < Type::UNDEFINED) t = Type::UNDEFINED;
	type = static_cast<Type>(t);
}

int Shader::gdGetType() const {
	return static_cast<int>(type);
}

void Shader::setType(Type t) {
	type = t;
}

Shader::Type Shader::getType() const {
	return type;
}

string Shader::getTypeString() const {
	return typeString(type);
}

string Shader::getFilename() const {
	return filename;
}

String Shader::gdGetFilename() const {
	return String(filename.c_str());
}

// ##############################

/*void Shader::loadSpirvFile(std::string spirv_filename) {
	filename = spirv_filename.c_str();

	std::ifstream file(filename, std::ios::ate | std::ios::binary);
	if (!file.is_open()) {
		Godot::print_error(
			String("Failed to open file: ") + String(filename),
			"void Shader::loadFile",
			"vulkan/src/shader.cpp", 0
		);

		return;
	}

	size_t size = static_cast<size_t>(file.tellg());
	code.resize(size);

	file.seekg(0);
	file.read(code.data(), size);
	file.close();
}*/

void Shader::loadSourceFile(String source_filename) {
	if (type == Type::UNDEFINED) {
		type = deduceType(source_filename.ascii().get_data());
	}

	filename = source_filename.ascii().get_data();

	std::ifstream file(filename, std::ios::ate);
	if (!file.is_open()) {
		Godot::print_error(
			String("Failed to open file: ") + String(filename.c_str()),
			"void Shader::loadSourceFile",
			"vulkan/src/shader.cpp", 80
		); return;
	}

	size_t size = file.tellg();
	source.clear();
	source.reserve(size);

	file.seekg(0);
	source.assign(
		(std::istreambuf_iterator<char>(file)),
		std::istreambuf_iterator<char>()
	);
	file.close();
}

void Shader::reloadSourceFile() {
	if (filename.empty()) {
		Godot::print_error(
			"Attempted to reload shader before a filename was set",
			"void Shader::reloadSourceFile", "shader.cpp", 0
		); return;
	}

	loadSourceFile(String(filename.c_str()));
}

bool Shader::compile() {
	shaderc::Compiler compiler;
	shaderc::CompilationResult<uint32_t> result = compiler.CompileGlslToSpv(
		source,
		typeShaderC(type),
		filename.c_str()
	);

	if (result.GetCompilationStatus() != shaderc_compilation_status_success) {
		errorString = result.GetErrorMessage();
		Godot::print_error(
			"Shader compilation error (" + String(filename.c_str()) + "): " +
			String(errorString.c_str()),
			"Shader::compile",
			"shader.cpp", 0
		);

		return false;
	}

	code.assign(result.begin(), result.end());
	return true;
}

String Shader::getCompilationStatusString() const {
	return String(errorString.c_str());
}

/*void Shader::saveSpirvFile(String filename) const {
	const char *data = filename.ascii().get_data();
	std::ofstream file(data, std::ios::out | std::ios::binary);
	if (!file.is_open()) {
		Godot::print_error(
			String("Failed to open file: ") + String(data),
			"void Shader::saveSpirvFile",
			"vulkan/src/shader.cpp", 135
		); return;
	}

	file.write(code.data(), code.size());
	file.close();
}*/

const vector<uint32_t>& Shader::getCode() const {
	return code;
}

// ##############################

void Shader::createModule() {
	if (module) destroyModule();

	if (code.empty()) {
		Godot::print_error(
			"Can't create module for a shader without code",
			"void Shader::createModule()",
			"vulkan/src/shader.cpp", 0
		); return;
	}

	vk::Device device = VKLib::getDevice();
	vk::ShaderModuleCreateInfo moduleInfo;
	moduleInfo.codeSize = code.size() * sizeof(uint32_t);
	moduleInfo.pCode = code.data();

	module = device.createShaderModule(moduleInfo);
	if (!module) {
		Godot::print_error(
			"Failed to create shader module",
			"void Shader::createModule()",
			"vulkan/src/shader.cpp", 0
		); return;
	}
}

void Shader::destroyModule() {
	if (!module) return;

	vk::Device device = VKLib::getDevice();
	device.destroyShaderModule(module);
}

vk::ShaderModule Shader::getModule() const {
	if (!module) {
		Godot::print_error(
			"Attempt to get vulkan shader module before its creation",
			"void Shader::getModule()",
			"vulkan/src/shader.cpp", 0
		);
	}

	return module;
}

// ##############################

string Shader::typeString(Shader::Type type) {
	switch (type) {
		case Type::VERTEX:    return "Vertex";    break;
		case Type::FRAGMENT:  return "Fragment";  break;
		default:              return "Undefined";
	}
}

vk::ShaderStageFlagBits Shader::typeVulkan(Shader::Type type) {
	switch (type) {
		case Type::VERTEX:
			return vk::ShaderStageFlagBits::eVertex;
			break;

		case Type::FRAGMENT:
			return vk::ShaderStageFlagBits::eFragment;
			break;

		default: return (vk::ShaderStageFlagBits)(0);
	}
}

shaderc_shader_kind Shader::typeShaderC(Shader::Type type) {
	switch (type) {
		case Type::VERTEX:
			return shaderc_vertex_shader;
			break;
		case Type::FRAGMENT:
			return shaderc_fragment_shader;
			break;
		default:
			return shaderc_glsl_infer_from_source;
	}
}

Shader::Type Shader::deduceType(std::string filename) {
	String name(filename.c_str());
	String vert("vert");
	String frag("frag");

	if (name.ends_with(vert)) return Type::VERTEX;
	if (name.ends_with(frag)) return Type::FRAGMENT;
	return Type::UNDEFINED;
}

// ##############################
