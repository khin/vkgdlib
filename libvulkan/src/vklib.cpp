#include "vklib.h"

#include <array>
#include <set>

#include <iostream>

#include <vulkan/vulkan.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>

#include "helper.h"

using namespace godot;
using std::vector;
using std::string;
using std::array;

// #################### //

VKLib* VKLib::singleton = nullptr;

VKLib::VKLib() {}
VKLib::~VKLib() {}

// ########## Godot stuff ########## //

void VKLib::_ready() {
	singleton = this;

	initWindow();
	initVulkan();

	camera.setProjectionParams(
		45.0f,
		extent.width / (float)extent.height,
		0.1f, 100.0f
	);

	camera.position = glm::vec3(0.0f, 2.0f, 5.0f);
	camera.target   = glm::vec3(0.0f, 0.0f, 0.0f);
}

void VKLib::_process(float delta) {
	if (!glfwWindowShouldClose(window)) {
		glfwPollEvents();
		input(delta);

		uniforms.delta = delta;
		uniforms.view = camera.getViewMatrix();
		uniforms.projection = camera.getProjectionMatrix();
		uniforms.cameraMatrix = camera.getCameraMatrix();
		uniforms.viewInverse = glm::inverse(uniforms.view);
		uniforms.projectionInverse = glm::inverse(uniforms.projection);

		for (Geometry *g : geometries) {
			g->updateUniformBuffer(uniforms);
		}

		drawFrame();
	}
}

void VKLib::_register_methods() {
	register_method("_ready", &VKLib::_ready);
	register_method("_process", &VKLib::_process);
	register_method("cleanup", &VKLib::cleanup);

	register_method("add_geometry", &VKLib::addGeometry);
	register_method("clear_command_buffer", &VKLib::clearCommandBuffers);
	register_method("create_command_buffer", &VKLib::allocateCommandBuffers);
}

void VKLib::cleanup() {
	device.waitIdle();

	// free depth resources
	device.destroyImageView(depthImageView);
	device.destroyImage(depthImage);
	device.freeMemory(depthImageMemory);

	device.destroySemaphore(semImageAvailable);
	device.destroySemaphore(semRenderFinished);

	device.destroyCommandPool(commandPool);

	for (auto framebuffer : framebuffers) { device.destroyFramebuffer(framebuffer); }
	framebuffers.clear();

	device.destroyRenderPass(renderpass);

	for (auto imageView : imageViews) { device.destroyImageView(imageView); }
	imageViews.clear();

	device.destroySwapchainKHR(swapchain);
	device.destroy();

	destroyDebugCallback();
	instance.destroySurfaceKHR(surface);
	instance.destroy();

	glfwDestroyWindow(window);
	glfwTerminate();
}

// ########## Vulkan stuff ########## //

VKLib* VKLib::getSingleton() {
	if (singleton == nullptr){
		Godot::print_error(
			"Attempt to get VKLib singleton before initialization",
			"getSingleton",
			"vklib.cpp", 37
		);
		return nullptr;
	}

	return singleton;
}

vk::PhysicalDevice VKLib::getPhysicalDevice() {
	if (singleton == nullptr) {
		Godot::print_error(
			"VKLib not initialized, cannot get physical device",
			"getPhysicalDevice",
			"vklib.cpp", 50
		);
		return vk::PhysicalDevice();
	}

	return singleton->physicalDevice;
}

vk::Device VKLib::getDevice() {
	if (singleton == nullptr) {
		Godot::print_error(
			"VKLib not initialized, cannot get logical device",
			"getDevice",
			"vklib.cpp", 64
		);
		return vk::Device();
	}

	return singleton->device;
}

vk::RenderPass VKLib::getRenderPass() const { return renderpass; }
vk::Viewport   VKLib::getViewport()   const { return viewport;   }
vk::Rect2D     VKLib::getScissor()    const { return scissor;    }

void VKLib::addGeometry(Object *object) {
	Geometry *geometry = as<Geometry>(object);

	if (geometry == nullptr) return;

	geometries.push_back(geometry);
}

// #################### //

VkQueueFamilyIndices::VkQueueFamilyIndices() {}

VkQueueFamilyIndices::VkQueueFamilyIndices(vk::PhysicalDevice device, vk::SurfaceKHR surface) {
	vector<vk::QueueFamilyProperties> families = device.getQueueFamilyProperties();

	int i = 0;
	for (const auto &family : families) {
		if (family.queueCount > 0 && family.queueFlags & vk::QueueFlagBits::eGraphics) {
			graphics = i;
		}

		bool present_support = device.getSurfaceSupportKHR(i, surface);
		if (family.queueCount > 0 && present_support) {
			present = i;
		}

		if (isComplete()) break;

		i++;
	}
}

// #################### //

SwapchainSupport::SwapchainSupport(){};

SwapchainSupport::SwapchainSupport(vk::PhysicalDevice device, vk::SurfaceKHR surface) {
	formats      = device.getSurfaceFormatsKHR(surface);
	capabilities = device.getSurfaceCapabilitiesKHR(surface);
	presentModes = device.getSurfacePresentModesKHR(surface);
}

// #################### //

void VKLib::initWindow() {
	glfwInit();
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
	glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);
	window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan", nullptr, nullptr);

	glfwSetMouseButtonCallback(window, &VKLib::mouseClickCallback);
	glfwSetCursorPosCallback(window, &VKLib::mouseMotionCallback);
}

bool VKLib::cameraMoving = false;
vec2 VKLib::lastMousePosition = { 0.0f, 0.0f };
void VKLib::mouseClickCallback(GLFWwindow *w, int button, int action, int mods) {
	if (!cameraMoving && button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS) {
		cameraMoving = true;

		double x, y; glfwGetCursorPos(w, &x, &y);
		lastMousePosition = vec2(x, y);

		glfwSetInputMode(w, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	} else if (cameraMoving && button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_RELEASE) {
		cameraMoving = false;
		glfwSetInputMode(w, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	}
}

void VKLib::mouseMotionCallback(GLFWwindow *w, double x, double y) {
	if (cameraMoving) {
		VKLib *vklib = VKLib::getSingleton();

		glm::vec2 position = glm::vec2(x, y);
		glm::vec2 offset = (position - lastMousePosition) * vklib->uniforms.delta * cameraAngularSpeed;
		vklib->camera.rotate(offset.x, offset.y);
		//vklib->camera.orbit(offset.x, offset.y); // orbiting ok, defining center is UI work, hard

		lastMousePosition = position;
	}
}

void VKLib::input(float delta) {
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
		glm::vec3 movement = camera.getDirection() * cameraSpeed * delta;
		camera.position += movement;
		camera.target += movement;
	}

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
		glm::vec3 left = glm::cross(camera.up, camera.getDirection());
		glm::vec3 movement = left * cameraSpeed * delta;
		camera.position += movement;
		camera.target += movement;
	}

	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
		glm::vec3 movement = camera.getDirection() * cameraSpeed * delta;
		camera.position -= movement;
		camera.target -= movement;
	}

	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
		glm::vec3 right = glm::cross(camera.getDirection(), camera.up);
		glm::vec3 offset = right * cameraSpeed * delta;
		camera.position += offset;
		camera.target += offset;
	}

	if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS) {
		glm::vec3 offset = camera.up * cameraSpeed * delta;
		camera.position -= offset;
		camera.target -= offset;
	}

	if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS) {
		glm::vec3 offset = camera.up * cameraSpeed * delta;
		camera.position += offset;
		camera.target += offset;
	}
}

void VKLib::initVulkan() {
	createVkInstance();

	#ifdef DEBUG_ENABLED
		setupDebugCallback();
	#endif

	createSurface();
	pickPhysicalDevice();
	createLogicalDevice();

	createSwapchain();
	createSwapchainImageViews();
	createRenderPass();

	createCommandPool();
	createDepthResources();
	createFramebuffers();

	allocateCommandBuffers();
	createSemaphores();
}

vector<const char*> VKLib::getRequiredExtensions() {
	vector<const char*> extensions;

	uint32_t glfw_extension_count = 0;
	const char** glfw_extensions;
	glfw_extensions = glfwGetRequiredInstanceExtensions(&glfw_extension_count);

	for (uint32_t i = 0; i < glfw_extension_count; i++) {
		extensions.push_back(glfw_extensions[i]);
	}

	#ifdef DEBUG_ENABLED
		extensions.push_back("VK_EXT_debug_report");
	#endif

	return extensions;
}

void VKLib::createVkInstance() {
	vk::ApplicationInfo app_info;
	app_info.pApplicationName = "VKLib";
	app_info.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
	app_info.apiVersion = VK_API_VERSION_1_0;

	vector<const char*> extensions = getRequiredExtensions();

	vk::InstanceCreateInfo info;
	info.pApplicationInfo = &app_info;
	info.enabledExtensionCount = static_cast<uint32_t>(extensions.size());
	info.ppEnabledExtensionNames = extensions.data();
	#ifdef DEBUG_ENABLED
		if (checkValidationLayersSupport()){
			info.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
			info.ppEnabledLayerNames = validationLayers.data();
		}
	#endif

	instance = vk::createInstance(info);
}

#ifdef DEBUG_ENABLED
bool VKLib::checkValidationLayersSupport() {
	vector<vk::LayerProperties> availableLayers;
	availableLayers = vk::enumerateInstanceLayerProperties();

	for (const char *layerName : validationLayers) {
		bool layerFound = false;

		for (const auto &layerProperties : availableLayers) {
			if (strcmp(layerName, layerProperties.layerName) == 0) {
				layerFound = true;
				break;
			}
		}

		if (!layerFound) {
			return false;
		}
	}

	return true;
}

static VKAPI_ATTR VkBool32 VKAPI_CALL _vk_debug_print(
		VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType,
		uint64_t obj, size_t location, int32_t code,
		const char *layerPrefix, const char *msg, void *userData) {
	String debugMessage = "\n\tVULKAN ERROR:\n\tFlags: ";
	debugMessage += String(vk::to_string(vk::DebugReportFlagBitsEXT(flags)).c_str());
	debugMessage += "\n\tObject type: " + String(vk::to_string(vk::DebugReportObjectTypeEXT(objType)).c_str());
	debugMessage += "\n\tCode: " + String::num(code);
	debugMessage += "\n\tMessage: " + String(msg);

	Godot::print_error(debugMessage, "_vk_debug_print", "vklib.cpp", 0);

	//std::cerr << debugMessage.ascii().get_data() << std::endl;

	return VK_FALSE;
}

bool VKLib::setupDebugCallback() {
	vk::DebugReportCallbackCreateInfoEXT debugInfo = {};
	debugInfo.flags =	vk::DebugReportFlagBitsEXT::eError |
				vk::DebugReportFlagBitsEXT::eWarning |
				vk::DebugReportFlagBitsEXT::ePerformanceWarning;
	debugInfo.pfnCallback = _vk_debug_print;

	auto vkCreateDebugReportCallbackEXT = (PFN_vkCreateDebugReportCallbackEXT)
			vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");

	if (vkCreateDebugReportCallbackEXT == nullptr) {
		Godot::print_error(
			"Failed to find procedure \"vkCreateDebugReportCallbackEXT\"",
			"setupDebugCallback", "vklib.cpp", 0
		); return false;
	}

	vkCreateDebugReportCallbackEXT(
			instance, (VkDebugReportCallbackCreateInfoEXT*) &debugInfo,
			nullptr, (VkDebugReportCallbackEXT*) &debugCallback);

	return true;

}

bool VKLib::destroyDebugCallback() {
	auto vkDestroyDebugReportCallbackEXT = (PFN_vkDestroyDebugReportCallbackEXT)
			vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");

	if (vkDestroyDebugReportCallbackEXT == nullptr) {
		Godot::print_error(
			"Failed to find procedure \"vkDestroyDebugReportCallbackEXT\"",
			"setupDebugCallback", "vklib.cpp", 0
		); return false;
	}

	vkDestroyDebugReportCallbackEXT(instance, (VkDebugReportCallbackEXT)debugCallback, nullptr);
	return true;
}

#endif // DEBUG_ENABLED

void VKLib::createSurface() {
	auto result = glfwCreateWindowSurface(instance, window, nullptr, (VkSurfaceKHR*)&surface);
	if (result != VK_SUCCESS) {
		throw std::runtime_error("Failed to create window surface");
	}
}

bool VKLib::checkDeviceExtensionsSupport(vk::PhysicalDevice device) {
	vector<vk::ExtensionProperties> availableExtensions;
	availableExtensions = device.enumerateDeviceExtensionProperties();

	std::set<string> requiredExtensions(deviceExtensions.begin(), deviceExtensions.end());

	for (const auto &extension : availableExtensions) {
		requiredExtensions.erase(extension.extensionName);
	}

	return requiredExtensions.empty();
}

bool VKLib::isDeviceSuitable(vk::PhysicalDevice device) {
	bool extensionsSupported = checkDeviceExtensionsSupport(device);
	VkQueueFamilyIndices indices(device, surface);

	bool swapchainAdequate = false;
	if (extensionsSupported) {
		SwapchainSupport swapchainSupport(device, surface);
		swapchainAdequate =
			!swapchainSupport.formats.empty() &&
			!swapchainSupport.presentModes.empty();
	}

	vk::PhysicalDeviceFeatures supportedFeatures;
	supportedFeatures = device.getFeatures();

	return extensionsSupported && indices.isComplete() &&
		swapchainAdequate && supportedFeatures.samplerAnisotropy;
}

void VKLib::pickPhysicalDevice() {
	vector<vk::PhysicalDevice> devices;
	devices = instance.enumeratePhysicalDevices();

	if (devices.empty()) {
		throw std::runtime_error("Failed to find GPU with Vulkan support!");
	}

	for (const auto& device : devices) {
		if (isDeviceSuitable(device)) {
			physicalDevice = device;
			break;
		}
	}

	if (!physicalDevice) {
		throw std::runtime_error("Failed to find a suitable GPU");
	}
}

void VKLib::createLogicalDevice() {
	VkQueueFamilyIndices indices(physicalDevice, surface);

	vector<vk::DeviceQueueCreateInfo> queue_infos;
	std::set<int> unique_queue_families = { indices.graphics, indices.present };

	float queue_priority = 1.0f;
	for (int queue_family : unique_queue_families) {
		vk::DeviceQueueCreateInfo queue_info;
		queue_info.queueFamilyIndex = queue_family;
		queue_info.queueCount = 1;
		queue_info.pQueuePriorities = &queue_priority;

		queue_infos.push_back(queue_info);
	}

	vk::PhysicalDeviceFeatures desiredFeatures;
	desiredFeatures.samplerAnisotropy = true;
	//vk::PhysicalDeviceFeatures deviceFeatures = physicalDevice.getFeatures();
	//desiredFeatures.sampleRateShading = deviceFeatures.sampleRateShading; // enables multisampling if present

	vk::DeviceCreateInfo deviceInfo;
	deviceInfo.queueCreateInfoCount = static_cast<uint32_t>(queue_infos.size());
	deviceInfo.pQueueCreateInfos = queue_infos.data();
	deviceInfo.pEnabledFeatures = &desiredFeatures;
	deviceInfo.enabledExtensionCount = static_cast<uint32_t>(deviceExtensions.size());
	deviceInfo.ppEnabledExtensionNames = deviceExtensions.data();
	#ifdef DEBUG_ENABLED
	deviceInfo.enabledLayerCount = static_cast<uint32_t>(validationLayers.size());
	deviceInfo.ppEnabledLayerNames = validationLayers.data();
	#endif

	device = physicalDevice.createDevice(deviceInfo);
	qGraphics = device.getQueue(indices.graphics, 0);
	qPresent  = device.getQueue(indices.present, 0);
}

// ##################################################

static vk::SurfaceFormatKHR chooseSwapchainSurfaceFormat(const vector<vk::SurfaceFormatKHR> &formats) {
	if (formats.empty()) return {};

	if (formats.size() == 1 and formats[0].format == vk::Format::eUndefined) {
		return { vk::Format::eB8G8R8A8Unorm, vk::ColorSpaceKHR::eSrgbNonlinear };
	}

	for (const auto& format : formats) {
		if (
			format.format == vk::Format::eB8G8R8A8Unorm and
			format.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear
		) {
			return format;
		}
	}

	return formats[0];
}

static vk::PresentModeKHR chooseSwapchainPresentMode(const vector<vk::PresentModeKHR> &modes) {
	for (const auto& mode : modes) {
		if (mode == vk::PresentModeKHR::eMailbox) {
			return mode;
		}
	}

	return vk::PresentModeKHR::eFifo;
}

static vk::Extent2D chooseSwapchainExtent(const vk::SurfaceCapabilitiesKHR &capabilities) {
	if (capabilities.currentExtent.width != std::numeric_limits<uint32_t>::max()) {
		return capabilities.currentExtent;
	}

	vk::Extent2D extent = { WIDTH, HEIGHT };
	extent.width = std::max(
		capabilities.minImageExtent.width,
		std::min(
			capabilities.maxImageExtent.width,
			extent.width
		)
	);

	extent.height = std::max(
		capabilities.minImageExtent.height,
		std::min(
			capabilities.maxImageExtent.height,
			extent.height
		)
	);

	return extent;
}

void VKLib::createSwapchain() {
	SwapchainSupport support(physicalDevice, surface);

	vk::SurfaceFormatKHR surfaceFormat = chooseSwapchainSurfaceFormat(support.formats);
	vk::PresentModeKHR presentMode = chooseSwapchainPresentMode(support.presentModes);
	extent = chooseSwapchainExtent(support.capabilities);
	format = surfaceFormat.format;

	uint32_t imageCount = support.capabilities.minImageCount + 1; // triple buffering if available
	if (support.capabilities.maxImageCount > 0 and support.capabilities.maxImageCount < imageCount) {
		// if max image count is 0, there is no limit (besides memory)
		imageCount = support.capabilities.maxImageCount;
	}

	VkQueueFamilyIndices indices(physicalDevice, surface);
	uint32_t queue_families[] = { (uint32_t)indices.graphics, (uint32_t)indices.present };

	vk::SwapchainCreateInfoKHR swapchainInfo;
	swapchainInfo.surface = surface;
	swapchainInfo.minImageCount = imageCount;
	swapchainInfo.imageFormat = surfaceFormat.format;
	swapchainInfo.imageColorSpace = surfaceFormat.colorSpace;
	swapchainInfo.imageExtent = extent;
	swapchainInfo.imageArrayLayers = 1; // >1 for stereoscopic 3D (VR)
	swapchainInfo.imageUsage = vk::ImageUsageFlagBits::eColorAttachment;
	swapchainInfo.imageSharingMode = vk::SharingMode::eExclusive;

	if (indices.graphics != indices.present) {
		swapchainInfo.imageSharingMode = vk::SharingMode::eConcurrent;
		swapchainInfo.queueFamilyIndexCount = 2;
		swapchainInfo.pQueueFamilyIndices = queue_families;
	}

	swapchainInfo.preTransform = support.capabilities.currentTransform;
	swapchainInfo.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;
	swapchainInfo.clipped = true;
	swapchainInfo.presentMode = presentMode;
	swapchainInfo.oldSwapchain = nullptr;

	swapchain = device.createSwapchainKHR(swapchainInfo);
	if (!swapchain) {
		throw std::runtime_error("Failed to create swapchain");
	}

	images = device.getSwapchainImagesKHR(swapchain);

	viewport.x = 0.0f;
	viewport.y = 0.0f;
	viewport.width  = (float) extent.width;
	viewport.height = (float) extent.height;
	viewport.minDepth = 0.0f;
	viewport.maxDepth = 1.0f;

	scissor.offset = vk::Offset2D(0, 0);
	scissor.extent = extent;
}

void VKLib::createSwapchainImageViews() {
	imageViews.resize(images.size());
	for (size_t i = 0; i < images.size(); i++) {
		imageViews[i] = createImageView(images[i], format);

		/*if (!imageViews[i]) {
			throw std::runtime_error("Failed to create swapchain imageview " + std::to_string(i));
		}*/
	}
}

void VKLib::createRenderPass() {
	vk::AttachmentDescription colorAttachment;
	colorAttachment.format  = format;
	colorAttachment.samples = vk::SampleCountFlagBits::e1;
	colorAttachment.loadOp  = vk::AttachmentLoadOp::eClear;
	colorAttachment.storeOp = vk::AttachmentStoreOp::eStore;
	colorAttachment.stencilLoadOp  = vk::AttachmentLoadOp::eDontCare;
	colorAttachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
	colorAttachment.initialLayout  = vk::ImageLayout::eUndefined;
	colorAttachment.finalLayout    = vk::ImageLayout::ePresentSrcKHR;

	vk::AttachmentDescription depthAttachment;
	depthAttachment.format  = vk::Format::eD32Sfloat;
	depthAttachment.samples = vk::SampleCountFlagBits::e1;
	depthAttachment.loadOp  = vk::AttachmentLoadOp::eClear;
	depthAttachment.storeOp = vk::AttachmentStoreOp::eDontCare;
	depthAttachment.stencilLoadOp  = vk::AttachmentLoadOp::eDontCare;
	depthAttachment.stencilStoreOp = vk::AttachmentStoreOp::eDontCare;
	depthAttachment.initialLayout  = vk::ImageLayout::eUndefined;
	depthAttachment.finalLayout    = vk::ImageLayout::eDepthStencilAttachmentOptimal;

	vk::AttachmentReference colorAttachmentRef;
	colorAttachmentRef.attachment = 0;
	colorAttachmentRef.layout = vk::ImageLayout::eColorAttachmentOptimal;

	vk::AttachmentReference depthAttachmentRef;
	depthAttachmentRef.attachment = 1;
	depthAttachmentRef.layout = vk::ImageLayout::eDepthStencilAttachmentOptimal;

	vk::SubpassDescription subpass;
	subpass.pipelineBindPoint = vk::PipelineBindPoint::eGraphics;
	subpass.colorAttachmentCount = 1;
	subpass.pColorAttachments = &colorAttachmentRef;
	subpass.pDepthStencilAttachment = &depthAttachmentRef;

	vk::SubpassDependency dependency;
	dependency.srcSubpass    = VK_SUBPASS_EXTERNAL;
	dependency.dstSubpass    = 0;
	dependency.srcStageMask  = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	dependency.srcAccessMask = (vk::AccessFlagBits)(0);
	dependency.dstStageMask  = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	dependency.dstAccessMask =	vk::AccessFlagBits::eColorAttachmentRead |
					vk::AccessFlagBits::eColorAttachmentWrite;

	array<vk::AttachmentDescription, 2> attachments = {
		colorAttachment, depthAttachment
	};

	vk::RenderPassCreateInfo renderpassInfo;
	renderpassInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
	renderpassInfo.pAttachments = attachments.data();
	renderpassInfo.subpassCount = 1;
	renderpassInfo.pSubpasses = &subpass;
	renderpassInfo.dependencyCount = 1;
	renderpassInfo.pDependencies = &dependency;

	renderpass = device.createRenderPass(renderpassInfo);
	if (!renderpass) {
		throw std::runtime_error("Failed to create renderpass object");
	}
}

void VKLib::createCommandPool() {
	VkQueueFamilyIndices indices(physicalDevice, surface);

	vk::CommandPoolCreateInfo poolInfo;
	poolInfo.queueFamilyIndex = indices.graphics;

	commandPool = device.createCommandPool(poolInfo);
}

void VKLib::createDepthResources() {
	vk::Format depthFormat = vk::Format::eD32Sfloat;

	vk::ImageCreateInfo imageInfo;
	imageInfo.imageType = vk::ImageType::e2D;
	imageInfo.extent.width = extent.width;
	imageInfo.extent.height = extent.height;
	imageInfo.extent.depth = 1;
	imageInfo.mipLevels = 1;
	imageInfo.arrayLayers = 1;
	imageInfo.format = depthFormat;
	imageInfo.tiling = vk::ImageTiling::eOptimal;
	imageInfo.initialLayout = vk::ImageLayout::eUndefined;
	imageInfo.usage = vk::ImageUsageFlagBits::eDepthStencilAttachment;
	imageInfo.sharingMode = vk::SharingMode::eExclusive;
	imageInfo.samples = vk::SampleCountFlagBits::e1;
	depthImage = device.createImage(imageInfo);

	vk::MemoryRequirements memRequirements;
	memRequirements = device.getImageMemoryRequirements(depthImage);

	vk::MemoryAllocateInfo allocInfo;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = findMemoryType(
		memRequirements.memoryTypeBits, vk::MemoryPropertyFlagBits::eDeviceLocal);

	depthImageMemory = device.allocateMemory(allocInfo);
	device.bindImageMemory(depthImage, depthImageMemory, 0);

	vk::ImageViewCreateInfo viewInfo;
	viewInfo.image = depthImage;
	viewInfo.viewType = vk::ImageViewType::e2D;
	viewInfo.format = depthFormat;
	viewInfo.subresourceRange.aspectMask = vk::ImageAspectFlagBits::eDepth;
	viewInfo.subresourceRange.baseMipLevel = 0;
	viewInfo.subresourceRange.levelCount = 1;
	viewInfo.subresourceRange.baseArrayLayer = 0;
	viewInfo.subresourceRange.layerCount = 1;

	depthImageView = device.createImageView(viewInfo);

	transitionImageLayout(
		depthImage, depthFormat,
		vk::ImageLayout::eUndefined,
		vk::ImageLayout::eDepthStencilAttachmentOptimal,
		vk::ImageAspectFlagBits::eDepth
	);
}

void VKLib::createFramebuffers() {
	framebuffers.resize(imageViews.size());
	for (size_t i = 0; i < imageViews.size(); i++) {
		array<vk::ImageView, 2> attachments = {
			imageViews[i], depthImageView
		};

		vk::FramebufferCreateInfo framebufferInfo;
		framebufferInfo.renderPass = renderpass;
		framebufferInfo.attachmentCount = static_cast<uint32_t>(attachments.size());
		framebufferInfo.pAttachments = attachments.data();
		framebufferInfo.width = extent.width;
		framebufferInfo.height = extent.height;
		framebufferInfo.layers = 1;

		framebuffers[i] = device.createFramebuffer(framebufferInfo);
		if (!framebuffers[i]) {
			throw std::runtime_error("Failed to create framebuffer " + std::to_string(i));
		}
	}
}

void VKLib::clearCommandBuffers() {
	if (!commandBuffers.empty()) {
		device.waitIdle();
		device.freeCommandBuffers(commandPool, commandBuffers);
		commandBuffers.clear();
	}
}

void VKLib::allocateCommandBuffers() {
	clearCommandBuffers();
	commandBuffers.resize(framebuffers.size());

	vk::CommandBufferAllocateInfo allocInfo;
	allocInfo.commandPool = commandPool;
	allocInfo.level = vk::CommandBufferLevel::ePrimary;
	allocInfo.commandBufferCount = static_cast<uint32_t>(commandBuffers.size());
	commandBuffers = device.allocateCommandBuffers(allocInfo);

	for (size_t i = 0; i < commandBuffers.size(); i++) {
		vk::CommandBufferBeginInfo beginInfo;
		beginInfo.flags = vk::CommandBufferUsageFlagBits::eSimultaneousUse;

		commandBuffers[i].begin(beginInfo);

		array<VkClearValue, 2> clearValues;
		clearValues[0].color = { 0.0f, 0.0f, 0.0f, 1.0f };
		clearValues[1].depthStencil = { 1.0f, 0 };

		vk::RenderPassBeginInfo passInfo;
		passInfo.renderPass = renderpass;
		passInfo.framebuffer = framebuffers[i];
		passInfo.renderArea.offset = vk::Offset2D(0, 0);
		passInfo.renderArea.extent = extent;
		passInfo.clearValueCount = static_cast<uint32_t>(clearValues.size());
		passInfo.pClearValues = (const vk::ClearValue*)clearValues.data();
		commandBuffers[i].beginRenderPass(passInfo, vk::SubpassContents::eInline);

		for(Geometry* g : geometries) {
			g->recordDrawingCommands(commandBuffers[i]);
		}

		commandBuffers[i].endRenderPass();
		commandBuffers[i].end();
	}
}

void VKLib::createSemaphores() {
	vk::SemaphoreCreateInfo semInfo;
	semImageAvailable = device.createSemaphore(semInfo);
	semRenderFinished = device.createSemaphore(semInfo);
}

void VKLib::drawFrame() {
	qPresent.waitIdle();

	uint32_t imageIndex;
	device.acquireNextImageKHR(swapchain, UINT64_MAX, semImageAvailable, nullptr, &imageIndex);

	vk::Semaphore waitSem[] = { semImageAvailable };
	vk::PipelineStageFlags waitStages[] = { vk::PipelineStageFlagBits::eColorAttachmentOutput };

	vk::Semaphore signalSem[] = { semRenderFinished };

	vk::SubmitInfo submitInfo;
	submitInfo.waitSemaphoreCount = 1;
	submitInfo.pWaitSemaphores = waitSem;
	submitInfo.pWaitDstStageMask = waitStages;
	submitInfo.commandBufferCount = 1;
	submitInfo.pCommandBuffers = &commandBuffers[imageIndex];
	submitInfo.signalSemaphoreCount = 1;
	submitInfo.pSignalSemaphores = signalSem;

	qGraphics.submit({ submitInfo }, nullptr);

	vk::SwapchainKHR swapchains[] = { swapchain };

	vk::PresentInfoKHR presentInfo;
	presentInfo.waitSemaphoreCount = 1;
	presentInfo.pWaitSemaphores = signalSem;
	presentInfo.swapchainCount = 1;
	presentInfo.pSwapchains = swapchains;
	presentInfo.pImageIndices = &imageIndex;

	qPresent.presentKHR(presentInfo);
}

vk::CommandBuffer VKLib::beginSingleTimeCommands() {
	vk::CommandBufferAllocateInfo alloc;
	alloc.level = vk::CommandBufferLevel::ePrimary;
	alloc.commandPool = commandPool;
	alloc.commandBufferCount = 1;

	vk::CommandBuffer commandBuffer;
	commandBuffer = device.allocateCommandBuffers(alloc)[0];

	vk::CommandBufferBeginInfo info;
	info.flags = vk::CommandBufferUsageFlagBits::eOneTimeSubmit;

	commandBuffer.begin(info);
	return commandBuffer;
}

void VKLib::endSingleTimeCommands(vk::CommandBuffer commandBuffer) {
	commandBuffer.end();

	vk::SubmitInfo info;
	info.commandBufferCount = 1;
	info.pCommandBuffers = &commandBuffer;

	qGraphics.submit({ info }, nullptr);
	qGraphics.waitIdle();

	device.freeCommandBuffers(commandPool, { commandBuffer });
}
