#pragma once

#include <vector>
#include <string>

#include <Godot.hpp>
#include <Image.hpp>

#include <vulkan/vulkan.hpp>

using byte = uint8_t;
using namespace godot;

vk::Format format_gd2vk(godot::Image::Format);

class Texture {//: GodotScript<Reference> {
public:
	Texture();
	~Texture();
	void cleanup();

	void setImage(const godot::Image*);
	void setImageData(const PoolByteArray &);
	void createTextureResources();

	vk::ImageView getImageView() const;
	vk::Sampler   getSampler()   const;

	std::string toString() const;

protected:
	void createTextureImage();
	void createSampler();

	std::vector<byte> data;
	uint32_t width;
	uint32_t height;
	uint32_t size;

	vk::Format format;

	vk::DeviceMemory memory;
	vk::Image        image;
	vk::ImageView    imageView;
	vk::Sampler      sampler;
};
