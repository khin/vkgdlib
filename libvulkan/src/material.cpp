#include "material.h"

#include <utility>
#include <spirv_cross.hpp>

#include "vklib.h"
#include "geometry.h"
#include "helper.h"

// ##############################

using namespace godot;
using std::string;
using std::vector;
using std::array;

namespace spvx = spirv_cross;

// ##############################

Material::Material()  {}
Material::~Material() {}

void Material::_register_methods() {
	register_method("set_default_texture_image", &Material::setDefaultTextureImage);

	register_method("set_name", &Material::setName);
	register_method("get_name", &Material::getName);

	register_method("set_shader", &Material::setShader);
	register_method("has_shader", &Material::hasShader);
	register_method("get_shader_name", &Material::getShaderName);

	register_method("setup", &Material::setup);
	register_method("print", &Material::printDebug);

	register_method("get_exported_uniforms", &Material::getExportedUniforms);
	register_method("set_exported_uniforms_values", &Material::setExportedUniformsValues);

	register_method("set_texture", &Material::setTexture);

	register_method("cleanup", &Material::cleanup);
}

// ##############################

void Material::setDefaultTextureImage(Object *image) {
	if (image == nullptr) {
		Godot::print_error(
			"Invalid argument: Object* is nullptr",
			"Material::setDefaultTextureImage", "material.cpp", 0
		); return;
	}

	godot::Image* _image = (godot::Image*) image;
	if (_image == nullptr) {
		Godot::print_error(
			"Invalid argument: Object* cannot be cast to Image*",
			"Material::setDefaultTextureImage", "material.cpp", 0
		); return;
	}

	defaultTexture = _image;
}

const godot::Image* Material::getDefaultTextureImage() const {
	return defaultTexture;
}

void Material::setShader(Object *shader_obj) {
	Shader *shader = as<Shader>(shader_obj);

	if (shader == nullptr) return;

	Shader::Type type = shader->getType();
	if (type == Shader::Type::UNDEFINED) {
		Godot::print_error(
			"Cannot assign a shader of undefined type to a material",
			"void Material::setShader",
			"material.cpp", 0
		); return;
	}

	shaders[type] = shader;
}

bool Material::hasShader(int type) const {
	return ((shaders.size() > (size_t)type) && (shaders[type] != nullptr));
}

String Material::getShaderName(int type) const {
	if (!hasShader(type)) {
		Godot::print_error(
			String("Material has no shader of type ") +
			String(Shader::typeString((Shader::Type)type).c_str()),
			"const Shader* Material::getShader",
			"material.cpp", 0
		); return "";
	}

	return shaders[type]->getName();
}

void Material::setName(String name) {
	this->name = name;
}

String Material::getName() const {
	return name;
}

// ##############################

vk::VertexInputBindingDescription Material::getBindingDescription() const {
	vk::VertexInputBindingDescription description;
	description.binding   = 0;
	description.stride    = attributesStride;
	description.inputRate = vk::VertexInputRate::eVertex;

	return description;
}

vector<vk::VertexInputAttributeDescription> Material::getAttributeDescriptions() const {
	vector<vk::VertexInputAttributeDescription> descriptions;

	for (const VertexAttribute &attribute : attributes) {
		vk::VertexInputAttributeDescription input;
		input.binding  = 0;
		input.location = attribute.location;
		input.format   = attribute.format;
		input.offset   = attribute.offset;

		descriptions.push_back(input);
	}

	return descriptions;
}

const vector<ImageSampler>&    Material::getSamplers()   const { return samplers;   }
const vector<UniformBuffer>&   Material::getUniforms()   const { return uniforms;   }
const vector<VertexAttribute>& Material::getAttributes() const { return attributes; }

uint32_t Material::getAttributesStride()   const { return attributesStride;   }
uint32_t Material::getUniformBuffersSize() const { return uniformBuffersSize; }

Array Material::getExportedUniforms() const { return exportedUniforms; }

void Material::setup(String material_name) {
	parseShaders();
	parseExportUniforms();

	createDescriptorSetLayout();
	createDescriptorPool();
	createPipeline();

	name = material_name;
}

void Material::parseShaders() {
	attributes.clear();
	uniforms.clear();

	for (auto &sampler : samplers) {
		sampler.texture.cleanup();
	}
	samplers.clear();

	for (const Shader *shader : shaders) {
		vector<uint32_t> spirv = shader->getCode();
		spirv_cross::Compiler glsl(std::move(spirv));
		spirv_cross::ShaderResources resources = glsl.get_shader_resources();

		// Parse shader attributes
		if (shader->getType() == Shader::Type::VERTEX) {
			for (auto &input : resources.stage_inputs) {
				spirv_cross::SPIRType t = glsl.get_type(input.type_id);

				VertexAttribute attribute;
				attribute.name = input.name;
				attribute.location = glsl.get_decoration(input.id, spv::DecorationLocation);

				switch (t.basetype) {
					case spirv_cross::SPIRType::BaseType::Int:
						attribute.size = t.vecsize * sizeof(int);

						if (t.vecsize == 1) {
							attribute.format = vk::Format::eR32Sint;
							attribute.type = VertexAttribute::Type::INT;

						} else if (t.vecsize == 2) { // ivec2
							attribute.format = vk::Format::eR32G32Sint;
							attribute.type = VertexAttribute::Type::IVEC2;

						} else if (t.vecsize == 3) { // ivec3
							attribute.format = vk::Format::eR32G32B32Sint;
							attribute.type = VertexAttribute::Type::IVEC3;
						}
						break;

					case spirv_cross::SPIRType::BaseType::UInt:
						attribute.size = t.vecsize * sizeof(uint);

						if (t.vecsize == 1) {
							attribute.format = vk::Format::eR32Uint;
							attribute.type = VertexAttribute::Type::UINT;

						} else if (t.vecsize == 2) { // uvec2
							attribute.format = vk::Format::eR32G32Uint;
							attribute.type = VertexAttribute::Type::UVEC2;

						} else if (t.vecsize == 3) { // uvec3
							attribute.format = vk::Format::eR32G32B32Uint;
							attribute.type = VertexAttribute::Type::UVEC3;
						}
						break;

					case spirv_cross::SPIRType::BaseType::Float:
						attribute.size = t.vecsize * sizeof(float);

						if (t.vecsize == 1) {
							attribute.format = vk::Format::eR32Sfloat;
							attribute.type = VertexAttribute::Type::FLOAT;

						} else if (t.vecsize == 2) { // vec2
							attribute.format = vk::Format::eR32G32Sfloat;
							attribute.type = VertexAttribute::Type::VEC2;

						} else if (t.vecsize == 3) { // vec3
							attribute.format = vk::Format::eR32G32B32Sfloat;
							attribute.type = VertexAttribute::Type::VEC3;

						} else if (t.vecsize == 4) { // vec4
							attribute.format = vk::Format::eR32G32B32A32Sfloat;
							attribute.type = VertexAttribute::Type::VEC4;
						}
						break;

					default:
						attribute.size = 0;
						attribute.type = VertexAttribute::Type::UNKNOWN;
						break;
				}

				attributes.push_back(attribute);
			}

			// make sure the attributes are ordered by location
			std::sort(attributes.begin(), attributes.end(),
				[](const VertexAttribute &a, const VertexAttribute &b) {
					return a.location < b.location;
				}
			);

			// calculate attributes offsets and overall stride
			uint32_t offset = 0;
			attributesStride = 0;
			for (auto &attribute : attributes) {
				attribute.offset = offset;
				offset += attribute.size;

				attributesStride += attribute.size;
			}
		}

		{ // Parse uniform buffers
			for (auto &uniform : resources.uniform_buffers) {
				spirv_cross::SPIRType t = glsl.get_type(uniform.type_id);
				spirv_cross::SPIRType bt = glsl.get_type(uniform.base_type_id);

				UniformBuffer ubo;
				ubo.name = uniform.name;
				ubo.size = glsl.get_declared_struct_size(t);
				ubo.binding  = glsl.get_decoration(uniform.id, spv::DecorationBinding);

				// Fill members info
				for (uint32_t i = 0; i < t.member_types.size(); i++) {
					spirv_cross::SPIRType mt = glsl.get_type(t.member_types[i]);

					// not a vector or a square matrix
					if (mt.vecsize != mt.columns && mt.columns != 1) { continue; }

					// mat2 not supported
					if (mt.columns == 2) { continue; }

					UBOMember member;
					member.name     = glsl.get_member_name(uniform.base_type_id, i);
					member.offset   = glsl.type_struct_member_offset(bt, i);
					member.size     = mt.vecsize * mt.columns;
					member.uboIndex = uniforms.size();

					switch (mt.basetype) {
						case spirv_cross::SPIRType::BaseType::Int:
						member.size *= sizeof(int);

						if (mt.vecsize == 1) {
							member.type = UBOMember::Type::INT;
						} else if (mt.vecsize == 2) {
							member.type = UBOMember::Type::IVEC2;
						} else if (mt.vecsize == 3 && mt.columns == 1) {
							member.type = UBOMember::Type::IVEC3;
						} else {
							member.type = UBOMember::Type::UNKNOWN;
						}
						break;

						case spirv_cross::SPIRType::BaseType::UInt:
						member.size *= sizeof(int);

						if (mt.vecsize == 1) {
							member.type = UBOMember::Type::UINT;
						} else if (mt.vecsize == 2) {
							member.type = UBOMember::Type::UVEC2;
						} else if (mt.vecsize == 3 && mt.columns == 1) {
							member.type = UBOMember::Type::UVEC3;
						} else {
							member.type = UBOMember::Type::UNKNOWN;
						}
						break;

						case spirv_cross::SPIRType::BaseType::Float:
						member.size *= sizeof(float);

						if (mt.vecsize == 1) {
							member.type = UBOMember::Type::FLOAT;
						} else if (mt.vecsize == 2) {
							member.type = UBOMember::Type::VEC2;
						} else if (mt.vecsize == 3 && mt.columns == 1) {
							member.type = UBOMember::Type::VEC3;
						} else if (mt.vecsize == 4 && mt.columns == 1) {
							member.type = UBOMember::Type::COLOR;
						} else if (mt.vecsize == 3) {
							member.type = UBOMember::Type::MAT3;
						} else if (mt.vecsize == 4) {
							member.type = UBOMember::Type::MAT4;
						} else {
							member.type = UBOMember::Type::UNKNOWN;
						}
						break;

						default:
						member.type = UBOMember::Type::UNKNOWN;
						member.size = 0;
						break;
					}

					ubo.members.push_back(member);
				}

				uniforms.push_back(ubo);
			}

			// make sure the uniforms are ordered by binding
			std::sort(uniforms.begin(), uniforms.end(),
				[](const UniformBuffer &a, const UniformBuffer &b) {
					return a.binding < b.binding;
				}
			);

			uint32_t offset = 0;
			uniformBuffersSize = 0;
			for (auto &uniform : uniforms) {
				uniform.offset = offset;
				offset += uniform.size;

				uniformBuffersSize += uniform.size;
			}
		}

		// Parse samplers
		for (auto &s : resources.sampled_images) {
			ImageSampler sampler;
			sampler.name     = s.name;
			sampler.binding  = glsl.get_decoration(s.id, spv::DecorationBinding);

			spirv_cross::SPIRType t = glsl.get_type(s.type_id);
			switch (t.image.dim) {
				case spv::Dim2D:
					sampler.type = ImageSampler::Type::SAMPLER2D;
					break;
				case spv::DimCube:
					sampler.type = ImageSampler::Type::CUBEMAP;
					break;
				default:
					sampler.type = ImageSampler::Type::UNKNOWN;
			}

			if (defaultTexture == nullptr) {
				Godot::print_error(
					String("This material has image samplers, but a default texture has not been set. ") +
					String("Consider a call to 'set_default_texture_image'"),
					"Material::parseShaders", "material.cpp", 0
				);
			} else {
				sampler.texture.setImage(defaultTexture);
			}

			samplers.push_back(sampler);
		}
	}
}

void Material::parseExportUniforms() {
	exportedUniforms.clear();

	for (const auto &uniform : uniforms) {
		for (const auto &member : uniform.members) {
			if ( // is a default application uniform
				member.name == "delta" ||
				member.name == "model" ||
				member.name == "view"  ||
				member.name == "projection" ||
				member.name == "modelView"  ||
				member.name == "modelViewProjection" ||
				member.name == "cameraMatrix" ||
				member.name == "normalMatrix" ||
				member.name == "modelInverse" ||
				member.name == "viewInverse"  ||
				member.name == "projectionInverse" ||
				member.name == "modelViewInverse"  ||
				member.name == "modelViewProjectionInverse"
			) continue;

			string member_type = UBOMember::type2string(member.type);

			Dictionary description;
			description["name"] = String(member.name.c_str());
			description["type"] = String(member_type.c_str());
			description["size"] = member.size;
			description["offset"] = member.offset;
			description["binding"] = uniform.binding;

			exportedUniforms.append(description);
		}
	}

	for (const auto &sampler : samplers) {
		string sampler_type = ImageSampler::type2string(sampler.type);

		Dictionary description;
		description["name"] = String(sampler.name.c_str());
		description["type"] = String(sampler_type.c_str());

		exportedUniforms.append(description);
	}
}

void Material::setExportedUniformsValues(Array values) {
	exportedUniforms = values;

	for (int i = 0; i < values.size(); i++) {
		Dictionary d = values[i];
		if (d["type"] == "texture") {
			String name = d["name"];
			godot::Image *srcImage = (godot::Image*)( (Object*) d["value"]);
			string id = name.ascii().get_data();

			try { getImageSampler(id).texture.setImage(srcImage); }
			catch (const std::invalid_argument &e) {
				Godot::print_error(
					e.what(),
					"Material::setExportedUniformsValues",
					"material.cpp", 0
				);
			}
		}
	}
}

void Material::createDescriptorSetLayout() {
	vector<vk::DescriptorSetLayoutBinding> bindings;

	for (const auto& ubo : uniforms) {
		vk::DescriptorSetLayoutBinding binding;
		binding.binding = ubo.binding;
		binding.descriptorType = vk::DescriptorType::eUniformBuffer;
		binding.descriptorCount = 1;
		binding.stageFlags = vk::ShaderStageFlagBits::eAllGraphics;

		bindings.push_back(binding);
	}

	for (const auto& sampler : samplers) {
		vk::DescriptorSetLayoutBinding binding;
		binding.binding = sampler.binding;
		binding.descriptorType = vk::DescriptorType::eCombinedImageSampler;
		binding.descriptorCount = 1;
		binding.stageFlags = vk::ShaderStageFlagBits::eAllGraphics;

		bindings.push_back(binding);
	}

	if (bindings.empty()) {
		Godot::print_error(
			"No bindings for this material", "void Material::createDescriptorSetLayout",
			"material.cpp", 0
		); return;
	}

	vk::DescriptorSetLayoutCreateInfo layoutInfo;
	layoutInfo.bindingCount = static_cast<uint32_t>(bindings.size());
	layoutInfo.pBindings = bindings.data();

	auto device = VKLib::getDevice();
	descriptorSetLayout = device.createDescriptorSetLayout(layoutInfo);
}

void Material::createDescriptorPool() {
	vector<vk::DescriptorPoolSize> poolSizes;

	if (uniforms.size() >= 1) {
		vk::DescriptorPoolSize uboPoolSize;
		uboPoolSize.type = vk::DescriptorType::eUniformBuffer;
		uboPoolSize.descriptorCount = static_cast<uint32_t>(uniforms.size());

		poolSizes.push_back(uboPoolSize);
	}

	if (samplers.size() >= 1) {
		vk::DescriptorPoolSize samplerPoolSize;
		samplerPoolSize.type = vk::DescriptorType::eCombinedImageSampler;
		samplerPoolSize.descriptorCount = static_cast<uint32_t>(samplers.size());

		poolSizes.push_back(samplerPoolSize);
	}

	if (poolSizes.empty()) return;

	vk::DescriptorPoolCreateInfo poolInfo;
	poolInfo.flags = vk::DescriptorPoolCreateFlagBits::eFreeDescriptorSet;
	poolInfo.poolSizeCount = static_cast<uint32_t>(poolSizes.size());
	poolInfo.pPoolSizes = poolSizes.data();
	poolInfo.maxSets = 32; // max number of objects that will share the material at a given time

	auto device = VKLib::getDevice();
	descriptorPool = device.createDescriptorPool(poolInfo);
}

void Material::createPipeline() {
	vk::Device device = VKLib::getDevice();

	vk::PipelineLayoutCreateInfo layoutInfo;
	layoutInfo.setLayoutCount = 1;
	layoutInfo.pSetLayouts = &descriptorSetLayout;
	layoutInfo.pushConstantRangeCount = 0;
	layoutInfo.pPushConstantRanges = nullptr;

	pipelineLayout = device.createPipelineLayout(layoutInfo);
	if (!pipelineLayout) {
		Godot::print_error(
			"Failed to create pipeline layout",
			"Material::createPipeline",
			"material.cpp", 0
		);
		throw std::runtime_error("Failed to create pipeline layout");
	}

	vk::RenderPass renderpass = VKLib::getSingleton()->getRenderPass();
	const vector<vk::Viewport> viewports = { VKLib::getSingleton()->getViewport() };
	const vector<vk::Rect2D>   scissors  = { VKLib::getSingleton()->getScissor()  };

	vector<vk::PipelineShaderStageCreateInfo> shaderStages;
	for (Shader *shader : shaders) {
		vk::PipelineShaderStageCreateInfo info;
		info.stage = Shader::typeVulkan(shader->getType());
		info.module = shader->getModule();
		info.pName = "main";

		shaderStages.push_back(info);
	}

	auto bindingDescription = getBindingDescription();
	auto attributeDescriptions = getAttributeDescriptions();

	vk::PipelineVertexInputStateCreateInfo inputInfo;
	inputInfo.vertexBindingDescriptionCount = 1;
	inputInfo.pVertexBindingDescriptions = &bindingDescription;
	inputInfo.vertexAttributeDescriptionCount = static_cast<uint32_t>(attributeDescriptions.size());
	inputInfo.pVertexAttributeDescriptions = attributeDescriptions.data();

	vk::PipelineInputAssemblyStateCreateInfo assemblyInfo;
	assemblyInfo.topology = vk::PrimitiveTopology::eTriangleList; // maybe make it configurable?
	assemblyInfo.primitiveRestartEnable = false;

	vk::PipelineViewportStateCreateInfo viewportInfo;
	viewportInfo.viewportCount = static_cast<uint32_t>(viewports.size());
	viewportInfo.pViewports = viewports.data();
	viewportInfo.scissorCount = static_cast<uint32_t>(scissors.size());
	viewportInfo.pScissors = scissors.data();

	vk::PipelineRasterizationStateCreateInfo rasterInfo; // make configurable?
	rasterInfo.depthClampEnable = false;
	rasterInfo.rasterizerDiscardEnable = false;
	rasterInfo.polygonMode = vk::PolygonMode::eFill;
	rasterInfo.lineWidth = 1.0f;
	rasterInfo.cullMode = vk::CullModeFlagBits::eNone;
	rasterInfo.frontFace = vk::FrontFace::eClockwise;
	rasterInfo.depthBiasEnable = false;

	vk::PipelineMultisampleStateCreateInfo multisampling;	// configurable?
	multisampling.sampleShadingEnable = false,
	multisampling.rasterizationSamples = vk::SampleCountFlagBits::e1;

	vk::PipelineDepthStencilStateCreateInfo depthInfo;
	depthInfo.depthTestEnable = true;
	depthInfo.depthWriteEnable = true;
	depthInfo.depthCompareOp = vk::CompareOp::eLess;
	depthInfo.depthBoundsTestEnable = false;
	depthInfo.stencilTestEnable = false;

	vk::PipelineColorBlendAttachmentState colorblendAttachment;
	colorblendAttachment.colorWriteMask =	vk::ColorComponentFlagBits::eR |
						vk::ColorComponentFlagBits::eG |
						vk::ColorComponentFlagBits::eB |
						vk::ColorComponentFlagBits::eA;
	colorblendAttachment.blendEnable = true;	// alpha blending
	colorblendAttachment.srcColorBlendFactor = vk::BlendFactor::eSrcAlpha;
	colorblendAttachment.dstColorBlendFactor = vk::BlendFactor::eOneMinusSrcAlpha;
	colorblendAttachment.colorBlendOp = vk::BlendOp::eAdd;
	colorblendAttachment.srcAlphaBlendFactor = vk::BlendFactor::eOne;
	colorblendAttachment.dstAlphaBlendFactor = vk::BlendFactor::eZero;
	colorblendAttachment.alphaBlendOp = vk::BlendOp::eAdd;

	vk::PipelineColorBlendStateCreateInfo colorblending;
	colorblending.logicOpEnable = false;
	colorblending.logicOp = vk::LogicOp::eCopy;
	colorblending.attachmentCount = 1;
	colorblending.pAttachments = &colorblendAttachment;
	colorblending.blendConstants[0] = 0.0f;
	colorblending.blendConstants[1] = 0.0f;
	colorblending.blendConstants[2] = 0.0f;
	colorblending.blendConstants[3] = 0.0f;

	vk::GraphicsPipelineCreateInfo pipelineInfo;
	pipelineInfo.stageCount = static_cast<uint32_t>(shaderStages.size());
	pipelineInfo.pStages    = shaderStages.data();
	pipelineInfo.pVertexInputState   = &inputInfo;
	pipelineInfo.pInputAssemblyState = &assemblyInfo;
	pipelineInfo.pViewportState      = &viewportInfo;
	pipelineInfo.pRasterizationState = &rasterInfo;
	pipelineInfo.pMultisampleState   = &multisampling;
	pipelineInfo.pDepthStencilState  = &depthInfo;
	pipelineInfo.pColorBlendState    = &colorblending;
	pipelineInfo.pDynamicState       = nullptr; // not used for now
	pipelineInfo.layout     = pipelineLayout;
	pipelineInfo.renderPass = renderpass;
	pipelineInfo.subpass    = 0;	// index of subpass inside renderpass
	pipelineInfo.basePipelineHandle = nullptr;
	pipelineInfo.basePipelineIndex  = -1;

	pipeline = device.createGraphicsPipeline(nullptr, pipelineInfo);
}

vk::Pipeline            Material::getPipeline()            { return pipeline;            }
vk::PipelineLayout      Material::getPipelineLayout()      { return pipelineLayout;      }
vk::DescriptorPool      Material::getDescriptorPool()      { return descriptorPool;      }
vk::DescriptorSetLayout Material::getDescriptorSetLayout() { return descriptorSetLayout; }

uint32_t Material::getUboCount()     const { return uniforms.size(); }
uint32_t Material::getSamplerCount() const { return samplers.size(); }

UniformBuffer& Material::getUniformBuffer(string name) {
	for (UniformBuffer &uniform : uniforms) {
		if (uniform.name == name) return uniform;
	}

	throw std::invalid_argument("This material has no uniform buffer called '" + name + "'");
}

ImageSampler& Material::getImageSampler(string name) {
	for (ImageSampler &sampler : samplers) {
		if (sampler.name == name) return sampler;
	}

	throw std::invalid_argument("This material has no image sampler called '" + name + "'");
}

void Material::setTexture(String samplerName, Object* object) {
	godot::Image* image = (godot::Image*)(object);
	if (image == nullptr) {
		Godot::print_error(
			"Invalid argument: Object* cannot be cast to Image*",
			"Material::setTexture", "material.cpp", 0
		); return;
	}

	try {
		ImageSampler &sampler = getImageSampler(samplerName.ascii().get_data());
		Godot::print("Setting image for sampler '" + String(sampler.name.c_str()) + "'");
		sampler.texture.setImage(image);

	} catch (const std::invalid_argument &e) {
		Godot::print_error(e.what(), "void Material::setTexture", "material.cpp", 0);
	}
}

void Material::cleanup() {
	for (auto& sampler : samplers) {
		sampler.texture.cleanup();
	}

	vk::Device device = VKLib::getDevice();

	device.destroyDescriptorPool(descriptorPool);
	device.destroyPipeline(pipeline);
	device.destroyPipelineLayout(pipelineLayout);
}

void Material::printDebug() const {
	string name = this->name.ascii().get_data();
	string info = "########## Material: " + name;
	info += "\nVertex shader inputs:\n";
	for (const VertexAttribute &attribute : attributes) {
		info += attribute.toString() + "\n";
	}

	info += "\nUniform buffers:\n";
	for (const UniformBuffer &uniform : uniforms) {
		info += uniform.toString() + "\n";
	}

	info += "\nImage samplers:\n";
	for (const ImageSampler &sampler : samplers) {
		info += sampler.toString() + "\n";
	}

	Godot::print(String(info.c_str()) + "\n");
}

// ##############################

string UBOMember::toString() const {
	string s;
	s =    "\tMember name: " + name;
	s += "\n\tType:        " + type2string(type);
	s += "\n\tSize:        " + std::to_string(size);
	s += "\n\tOffset:      " + std::to_string(offset);
	return s;
}

string UBOMember::type2string(UBOMember::Type t) {
	string s;

	switch (t) {
		case Type::INT:   s = "int";   break;
		case Type::UINT:  s = "uint";  break;
		case Type::FLOAT: s = "float"; break;
		case Type::IVEC2: s = "ivec2"; break;
		case Type::IVEC3: s = "ivec3"; break;
		case Type::UVEC2: s = "uvec2"; break;
		case Type::UVEC3: s = "uvec3"; break;
		case Type::VEC2:  s = "vec2";  break;
		case Type::VEC3:  s = "vec3";  break;
		case Type::COLOR: s = "color"; break;
		case Type::MAT3:  s = "mat3";  break;
		case Type::MAT4:  s = "mat4";  break;
		default: s = "Unknown";
	}

	return s;
}

string UniformBuffer::toString() const {
	string s;
	s +=   "Uniform name: " + name;
	s += "\nBinding:      " + std::to_string(binding);
	s += "\nSize:         " + std::to_string(size);
	s += "\nOffset:       " + std::to_string(offset);

	if (!members.empty()) {
		s += "\nMembers:\n";
		for (const UBOMember &member : members) {
			s += member.toString() + "\n\n";
		}
	}

	return s;
}

string ImageSampler::toString() const {
	string s;
	s +=   "Sampler name: " + name;
	s += "\nBinding:      " + std::to_string(binding);
	s += "\nType:         " + type2string(type);
	s += "\n"               + texture.toString();

	return s;
}

string ImageSampler::type2string(ImageSampler::Type t) {
	string s;

	switch (t) {
		case Type::SAMPLER2D: s = "sampler2D";   break;
		case Type::CUBEMAP:   s = "samplerCube"; break;
		default: s = "Unknown";
	}

	return s;
}

string VertexAttribute::toString() const {
	string s;
	s +=   "Name:        " + name;
	s += "\nType:        " + type2string(type);
	s += "\nLocation:    " + std::to_string(location);
	return s;
}

string VertexAttribute::type2string(VertexAttribute::Type t) {
	string s;

	switch (t) {
		case Type::INT:   s = "int";   break;
		case Type::UINT:  s = "uint";  break;
		case Type::FLOAT: s = "float"; break;
		case Type::IVEC2: s = "ivec2"; break;
		case Type::IVEC3: s = "ivec3"; break;
		case Type::UVEC2: s = "uvec2"; break;
		case Type::UVEC3: s = "uvec3"; break;
		case Type::VEC2:  s = "vec2";  break;
		case Type::VEC3:  s = "vec3";  break;
		case Type::VEC4:  s = "vec4";  break;
		default: s = "Unknown";
	}

	return s;
}
