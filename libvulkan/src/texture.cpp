#include "texture.h"

#include <vulkan/vulkan.hpp>

#include "vklib.h"
#include "helper.h"

using namespace godot;

Texture::Texture() {
	width  = 0;
	height = 0;
	size   = 0;
	format = vk::Format::eUndefined;
}

Texture::~Texture() {}

void Texture::setImage(const godot::Image *image) {
	if (image == nullptr) {
		Godot::print_error(
			"Invalid parameter (Image*): nullptr",
			"void Texture::setImage", "texture.cpp", 0
		); return;
	}

	vk::Format _format = format_gd2vk(image->get_format());
	if (_format == vk::Format::eUndefined) {
		Godot::print_error(
			"Could not infer vulkan image type from Godot image type",
			"Texture::setImage",
			"texture.cpp", 0
		); return;
	}

	width  = static_cast<uint32_t>(image->get_width());
	height = static_cast<uint32_t>(image->get_height());
	format = _format;

	auto device = VKLib::getDevice();
	device.waitIdle();

	setImageData(image->get_data());
	createTextureResources();
}

void Texture::setImageData(const PoolByteArray &imageData) {
	cleanup();
	size = imageData.size();
	data.reserve(size);

	auto imageRead = imageData.read();
	data.assign(imageRead.ptr(), imageRead.ptr() + size);
}

void Texture::createTextureResources() {
	createTextureImage();
	imageView = createImageView(image, format);
	createSampler();
}

void Texture::createTextureImage() {
	vk::Buffer staging;
	vk::DeviceMemory stagingMem;

	staging = createBuffer(vk::BufferUsageFlagBits::eTransferSrc, size, stagingMem);
	fillBufferMemory(stagingMem, size, data.data());

	image = createImage(
		width, height, format,
		vk::ImageTiling::eOptimal,
		vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled,
		vk::MemoryPropertyFlagBits::eDeviceLocal,
		memory
	);

	transitionImageLayout(
		image, format,
		vk::ImageLayout::eUndefined,
		vk::ImageLayout::eTransferDstOptimal
	);

	copyBufferToImage(staging, image, width, height);

	transitionImageLayout(
		image, format,
		vk::ImageLayout::eTransferDstOptimal,
		vk::ImageLayout::eShaderReadOnlyOptimal
	);

	auto device = VKLib::getDevice();
	device.destroyBuffer(staging);
	device.freeMemory(stagingMem);
}

void Texture::createSampler() {
	vk::SamplerCreateInfo info;
	info.magFilter = vk::Filter::eLinear;
	info.minFilter = vk::Filter::eLinear;
	info.addressModeU = vk::SamplerAddressMode::eRepeat;
	info.addressModeV = vk::SamplerAddressMode::eRepeat;
	info.addressModeW = vk::SamplerAddressMode::eRepeat;
	info.anisotropyEnable = true;
	info.maxAnisotropy = 16;
	info.borderColor = vk::BorderColor::eIntOpaqueBlack;
	info.unnormalizedCoordinates = false;
	info.compareEnable = false;
	info.compareOp = vk::CompareOp::eAlways;
	info.mipmapMode = vk::SamplerMipmapMode::eLinear;
	info.mipLodBias = 0.0f;
	info.minLod = 0.0f;
	info.maxLod = 0.0f;

	sampler = VKLib::getDevice().createSampler(info);
	if (!sampler) {
		Godot::print_error(
			"Failed to create texture sampler",
			"void Texture::createSampler", "texture.cpp", 0
		);
	}
}

std::string Texture::toString() const {
	std::string s;
	s += "Texture:";
	s += "\n\tWidth:  " + std::to_string(width);
	s += "\n\tHeight: " + std::to_string(height);
	s += "\n\tSize:   " + std::to_string(size);
	s += "\n\tFormat: " +  vk::to_string(format);
	s += "\n\n";

	return s;
}

vk::ImageView Texture::getImageView() const { return imageView; }
vk::Sampler   Texture::getSampler()   const { return sampler;   }

void Texture::cleanup() {
	data.clear();

	auto device = VKLib::getDevice();
	device.waitIdle();

	if (sampler)   device.destroySampler(sampler);
	if (imageView) device.destroyImageView(imageView);
	if (image)     device.destroyImage(image);
	if (memory)    device.freeMemory(memory);
}

vk::Format format_gd2vk(godot::Image::Format format) {
	switch (format) {
		case Image::Format::FORMAT_RGB8:        return vk::Format::eR8G8B8Unorm;            break;
		case Image::Format::FORMAT_RGBA8:       return vk::Format::eR8G8B8A8Unorm;          break;
		case Image::Format::FORMAT_ETC2_RGB8:   return vk::Format::eEtc2R8G8B8UnormBlock;   break;
		case Image::Format::FORMAT_ETC2_RGBA8:  return vk::Format::eEtc2R8G8B8A8UnormBlock; break;
		case Image::Format::FORMAT_ETC2_RGB8A1: return vk::Format::eEtc2R8G8B8A1UnormBlock; break;
		default:                                return vk::Format::eUndefined;              break;
	}
}
