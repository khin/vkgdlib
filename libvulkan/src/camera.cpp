#include "camera.h"

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/quaternion.hpp>

using namespace glm;

Camera::Camera() {}
Camera::Camera(float aspectRatio) {
	position = vec3(0.0f, 2.0f, 5.0f);
	target   = vec3(0.0f, 0.0f, 0.0f);

	this->setProjectionParams(45.0f, aspectRatio, 0.1f, 100.0f);
}

void Camera::setDirection(vec3 direction) {
	target = position + direction;
}

vec3 Camera::getDirection() const {
	return glm::normalize(target - position);
}

void Camera::setProjectionParams(float fov, float aspect, float near, float far) {
	projection = glm::perspective(glm::radians(fov), aspect, near, far);
	projection[1][1] *= -1.0f;
}

mat4 Camera::getViewMatrix() const {
	return glm::lookAt(position, target, up);
}

mat4 Camera::getProjectionMatrix() const {
	return projection;
}

mat4 Camera::getCameraMatrix() const {
	return projection * getViewMatrix();
}

void Camera::rotate(float angleX, float angleY) {
	quat rotation = glm::angleAxis(glm::radians(-angleX), up);
	rotation = glm::rotate(rotation, glm::radians(angleY), glm::cross(up, getDirection()));
	rotation = glm::normalize(rotation);

	setDirection(rotation * getDirection());
}

void Camera::orbit(float angleX, float angleY) {
	quat rotation = glm::angleAxis(glm::radians(-angleX), up);
	rotation = glm::rotate(rotation, glm::radians(angleY), glm::cross(up, getDirection()));
	rotation = glm::normalize(rotation);

	vec3 relative_position = (position - target);
	relative_position = rotation * relative_position;

	position = relative_position + target;
}
