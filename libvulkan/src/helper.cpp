#include "helper.h"
#include "vklib.h"

#include <stdexcept>

uint32_t findMemoryType(uint32_t type_filter, vk::MemoryPropertyFlags properties) {
	vk::PhysicalDeviceMemoryProperties memProperties;
	memProperties = VKLib::getPhysicalDevice().getMemoryProperties();

	for (uint32_t i = 0; i < memProperties.memoryTypeCount; i++) {
		if (
			type_filter & (1 << i) and
			(memProperties.memoryTypes[i].propertyFlags & properties) == properties
		) {
			return i;
		}
	}

	throw std::runtime_error("Failed to find suitable memory type");
}

vk::Buffer createBuffer(
	vk::BufferUsageFlags usage,
	size_t size,
	vk::DeviceMemory &memory,
	vk::SharingMode sharingMode,
	vk::MemoryPropertyFlags memPropertyFlags
) {
	vk::BufferCreateInfo bufferInfo;
	bufferInfo.size = size;
	bufferInfo.usage = usage;
	bufferInfo.sharingMode = sharingMode;

	auto device = VKLib::getDevice();
	vk::Buffer buffer = device.createBuffer(bufferInfo);

	if (!buffer) {
		Godot::print_error(
			"Failed to create vulkan buffer object",
			"vk::Image createBuffer", "helper.cpp", 0
		); return nullptr;
	}

	vk::MemoryRequirements memRequirements;
	memRequirements = device.getBufferMemoryRequirements(buffer);

	vk::MemoryAllocateInfo allocInfo;
	allocInfo.allocationSize = memRequirements.size;
	allocInfo.memoryTypeIndex = findMemoryType(
		memRequirements.memoryTypeBits,
		memPropertyFlags
	);

	memory = device.allocateMemory(allocInfo);

	if (!memory) {
		Godot::print_error(
			"Failed to allocate memory for vulkan buffer object",
			"vk::Image createBuffer", "helper.cpp", 0
		); return nullptr;
	}

	device.bindBufferMemory(buffer, memory, 0);
	return buffer;
}

void fillBufferMemory(vk::DeviceMemory memory, size_t size, const void *data) {
	vk::Device device = VKLib::getDevice();

	void *ptr = device.mapMemory(memory, 0, size);
	std::memcpy(ptr, data, size);
	device.unmapMemory(memory);
}

void copyBuffer(vk::Buffer source, vk::Buffer destination, vk::DeviceSize size) {
	vk::CommandBuffer commandBuffer = VKLib::getSingleton()->beginSingleTimeCommands();

	vk::BufferCopy region;
	region.size = size;

	commandBuffer.copyBuffer(source, destination, { region });

	VKLib::getSingleton()->endSingleTimeCommands(commandBuffer);
}

vk::Image createImage(
	uint32_t width, uint32_t height,
	vk::Format format,
	vk::ImageTiling tiling,
	vk::ImageUsageFlags usage,
	vk::MemoryPropertyFlags properties,
	vk::DeviceMemory& memory
) {
	vk::Image image;
	auto device = VKLib::getDevice();

	vk::ImageCreateInfo info;
	info.imageType     = vk::ImageType::e2D;
	info.extent.width  = width;
	info.extent.height = height;
	info.extent.depth  = 1;
	info.mipLevels     = 1;
	info.arrayLayers   = 1;
	info.format  = format;
	info.tiling  = tiling;
	info.usage   = usage;
	info.samples = vk::SampleCountFlagBits::e1;
	info.sharingMode   = vk::SharingMode::eExclusive;
	info.initialLayout = vk::ImageLayout::eUndefined;

	image = device.createImage(info);
	if (!image) {
		Godot::print_error(
			"Failed to create vulkan image object",
			"vk::Image createImage", "helper.cpp", 0
		); return nullptr;
	}

	vk::MemoryRequirements memreq;
	memreq = device.getImageMemoryRequirements(image);

	vk::MemoryAllocateInfo alloc;
	alloc.allocationSize  = memreq.size;
	alloc.memoryTypeIndex = findMemoryType(memreq.memoryTypeBits, properties);

	memory = device.allocateMemory(alloc);
	if (!memory) {
		Godot::print_error(
			"Failed to allocate memory for vulkan image object",
			"vk::Image createImage", "helper.cpp", 0
		); return nullptr;
	}

	device.bindImageMemory(image, memory, 0);

	return image;
}

vk::ImageView createImageView(vk::Image image, vk::Format format) {
	vk::ImageViewCreateInfo info;
	info.image    = image;
	info.viewType = vk::ImageViewType::e2D;
	info.format   = format;
	info.subresourceRange.aspectMask     = vk::ImageAspectFlagBits::eColor;
	info.subresourceRange.baseMipLevel   = 0;
	info.subresourceRange.levelCount     = 1;
	info.subresourceRange.baseArrayLayer = 0;
	info.subresourceRange.layerCount     = 1;

	vk::ImageView imageView = VKLib::getDevice().createImageView(info);
	if (!imageView) {
		Godot::print_error(
			"Failed to create texture image view",
			"vk::ImageView createImageView", "helper.cpp", 0
		); return nullptr;
	}

	return imageView;
}

void transitionImageLayout(
	vk::Image image, vk::Format format,
	vk::ImageLayout oldLayout, vk::ImageLayout newLayout,
	vk::ImageAspectFlags aspect
) {
	vk::PipelineStageFlags sourceStage;
	vk::PipelineStageFlags destinationStage;

	vk::CommandBuffer commandBuffer = VKLib::getSingleton()->beginSingleTimeCommands();

	vk::ImageMemoryBarrier barrier;
	barrier.oldLayout = oldLayout;
	barrier.newLayout = newLayout;
	barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
	barrier.image = image;
	barrier.subresourceRange.aspectMask     = aspect;
	barrier.subresourceRange.baseMipLevel   = 0;
	barrier.subresourceRange.levelCount     = 1;
	barrier.subresourceRange.baseArrayLayer = 0;
	barrier.subresourceRange.layerCount     = 1;

	if (
		oldLayout == vk::ImageLayout::eUndefined &&
		newLayout == vk::ImageLayout::eTransferDstOptimal
	) {
		barrier.srcAccessMask = static_cast<vk::AccessFlagBits>(0);
		barrier.dstAccessMask = vk::AccessFlagBits::eTransferWrite;
		sourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
		destinationStage = vk::PipelineStageFlagBits::eTransfer;
	} else if (
		oldLayout == vk::ImageLayout::eTransferDstOptimal &&
		newLayout == vk::ImageLayout::eShaderReadOnlyOptimal
	) {
		barrier.srcAccessMask = vk::AccessFlagBits::eTransferWrite;
		barrier.dstAccessMask = vk::AccessFlagBits::eShaderRead;
		sourceStage = vk::PipelineStageFlagBits::eTransfer;
		destinationStage = vk::PipelineStageFlagBits::eFragmentShader;
	} else if (
		oldLayout == vk::ImageLayout::eUndefined &&
		newLayout == vk::ImageLayout::eDepthStencilAttachmentOptimal
	) {
		barrier.dstAccessMask =
			vk::AccessFlagBits::eDepthStencilAttachmentRead |
			vk::AccessFlagBits::eDepthStencilAttachmentWrite;
		sourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
		destinationStage = vk::PipelineStageFlagBits::eEarlyFragmentTests;
	} else {
		Godot::print_error(
			"Unsupported image layout transition",
			"void transitionImageLayout", "helper.cpp", 0
		); return;
	}

	commandBuffer.pipelineBarrier(
		sourceStage, destinationStage,
		static_cast<vk::DependencyFlagBits>(0),  // DependencyFlags
		{}, // MemoryBarrier
		{}, // BufferMemoryBarrier
		{ barrier } // ImageMemoryBarrier
	);

	VKLib::getSingleton()->endSingleTimeCommands(commandBuffer);
}

void copyBufferToImage(vk::Buffer buffer, vk::Image image, uint32_t width, uint32_t height) {
	vk::CommandBuffer commandBuffer = VKLib::getSingleton()->beginSingleTimeCommands();

	vk::BufferImageCopy region;
	region.bufferOffset = 0;
	region.bufferRowLength = 0;
	region.bufferImageHeight = 0;

	region.imageSubresource.aspectMask = vk::ImageAspectFlagBits::eColor;
	region.imageSubresource.mipLevel = 0;
	region.imageSubresource.baseArrayLayer = 0;
	region.imageSubresource.layerCount = 1;

	region.imageOffset = vk::Offset3D(0, 0, 0);
	region.imageExtent = vk::Extent3D(width, height, 1);

	commandBuffer.copyBufferToImage(buffer, image, vk::ImageLayout::eTransferDstOptimal, { region });

	VKLib::getSingleton()->endSingleTimeCommands(commandBuffer);
}
