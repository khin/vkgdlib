#include "geometry.h"

#include <ArrayMesh.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/matrix_inverse.hpp>
#include <glm/gtc/quaternion.hpp>

#include "vklib.h"
#include "texture.h"
#include "helper.h"

using namespace godot;
using namespace glm;
using std::vector;
using std::array;

// ##################################################

Geometry::~Geometry() {}

Geometry::Geometry() {
	position = { 0.0f, 0.0f, 0.0f };
	rotation = { 0.0f, 0.0f, 0.0f };
	scale = { 1.0f, 1.0f, 1.0f };

	modelMatrix = mat4(1.0f);
	indexed = true;
	material = nullptr;
}

void Geometry::_register_methods() {
	register_method("set_array_mesh", &Geometry::setArrayMesh);
	register_method("set_material", &Geometry::setMaterial);
	register_method("get_material_name", &Geometry::getMaterialName);
	register_method("create_descriptor_set", &Geometry::createDescriptorSet);
	register_method("cleanup", &Geometry::cleanup);

	register_property("position", &Geometry::setPosition, &Geometry::getPosition, Vector3(0.0f, 0.0f, 0.0f));
	register_property("rotation", &Geometry::setRotation, &Geometry::getRotation, Vector3(0.0f, 0.0f, 0.0f));
	register_property("scale", &Geometry::setScale, &Geometry::getScale, Vector3(1.0f, 1.0f, 1.0f));
}

void Geometry::setPosition(Vector3 p) {	position = { p.x, p.y, p.z }; updateModelMatrix(); }
void Geometry::setRotation(Vector3 r) { rotation = { r.x, r.y, r.z }; updateModelMatrix(); }
void Geometry::setScale(Vector3 s) { scale = { s.x, s.y, s.z }; updateModelMatrix(); }

void Geometry::updateModelMatrix() {
	mat4 translation = glm::translate(mat4(1.0f), this->position);
	mat4 scale = glm::scale(mat4(1.0f), this->scale);

	quat rot = quat(this->rotation);
	mat4 rotation = glm::mat4_cast(rot);

	modelMatrix = translation * rotation * scale;
}

Vector3 Geometry::getPosition() const { return Vector3(position.x, position.y, position.z); }
Vector3 Geometry::getRotation() const { return Vector3(rotation.x, rotation.y, rotation.z); }
Vector3 Geometry::getScale() const { return Vector3(scale.x, scale.y, scale.z); }

void Geometry::setArrayMesh(Object* obj) {
	ArrayMesh *mesh = (ArrayMesh*)(obj);
	if (mesh == nullptr) {
		Godot::print_error(
			"Invalid argument: Object* cannot be cast to ArrayMesh*",
			"void Geometry::setArrayMesh", "geometry.cpp", 0
		); return;
	}

	vertices.clear();
	normals.clear();
	tangents.clear();
	uvs.clear();
	indices.clear();
	indexed = false;

	int64_t surface_count = mesh->get_surface_count();
	for (int64_t i = 0; i < surface_count; i++) {
		Array meshArrays = mesh->surface_get_arrays(i);
		int format = mesh->surface_get_format(i);

		PoolVector3Array array = meshArrays[ArrayMesh::ARRAY_VERTEX];
		for (int64_t vi = 0; vi < array.size(); vi++) {
			Vector3 v = array[vi];
			vertices.push_back( { v.x, v.y, v.z } );
		}

		if (format & ArrayMesh::ARRAY_FORMAT_NORMAL) {
			PoolVector3Array array = meshArrays[ArrayMesh::ARRAY_NORMAL];
			for (int64_t ni = 0; ni < array.size(); ni++) {
				Vector3 n = array[ni];
				normals.push_back( { n.x, n.y, n.z } );
			}
		}

		if (format & ArrayMesh::ARRAY_FORMAT_TANGENT) {
			Array array = meshArrays[ArrayMesh::ARRAY_TANGENT];
			for (int64_t ti = 0; ti < array.size(); ti += 4) {
				PoolRealArray t = array[ti];
				tangents.push_back( { array[ti], array[ti + 1], array[ti + 2], array[ti + 3] } );
			}
		}

		if (format & ArrayMesh::ARRAY_FORMAT_TEX_UV) {
			PoolVector2Array array = meshArrays[ArrayMesh::ARRAY_TEX_UV];
			for (int64_t uvi = 0; uvi < array.size(); uvi++) {
				Vector2 uv = array[uvi];
				uvs.push_back( { uv.x, uv.y } );
			}
		}

		if (format & ArrayMesh::ARRAY_FORMAT_INDEX) {
			PoolIntArray array = meshArrays[ArrayMesh::ARRAY_INDEX];
			for (int64_t ii = 0; ii < array.size(); ii++) {
				uint32_t index = static_cast<uint32_t>(array[ii]);
				indices.push_back(index);
			}

			indexed = true;
		}

	}
}

void Geometry::setMaterial(Object *o) {
	// Godot also defines a godot::Material class, so I need the :: thingies
	::Material *m = as<::Material>(o);

	if (m == nullptr) {
		Godot::print_error(
			"Parameter is not a Material object",
			"setMaterial",
			"geometry.cpp", 0
		); return;
	}

	if (material) {
		auto device = VKLib::getDevice();
		device.waitIdle();

		if (descriptorSet) {
			Godot::print("Freeing allocated descriptor set before assigning new material to geometry");
			device.freeDescriptorSets(/*material->getDescriptorPool()*/poolFrom, { descriptorSet });
		}

		if (!uniformBuffers.empty()) {
			Godot::print("Freeing allocated uniform buffers before assigning new material to geometry");

			for (auto &pair : uniformBuffers) {
				UniformBufferData &ubo = pair.second;
				device.destroyBuffer(ubo.buffer);
				device.freeMemory(ubo.memory);
			}

			uniformBuffers.clear();
		}
	}

	material = m;
	createBuffers();
}

String Geometry::getMaterialName() const {
	if (!material) return "";
	return material->getName();
}

void Geometry::createBuffers() {
	createVertexBuffer();
	createIndexBuffer();
	createUniformBuffer();
}

void Geometry::createVertexBuffer() {
	if (material == nullptr) {
		Godot::print_error(
			"Attempted to create vertex buffer but material was not set yet",
			"void::Geometry::createVertexBuffer", "geometry.cpp", 0
		); return;
	}

	vertexData.clear();
	for (uint32_t i = 0; i < vertices.size(); i++) {
		for (const VertexAttribute &attribute : material->getAttributes()) {
			if (attribute.name == "position" && (
					attribute.type == VertexAttribute::Type::VEC3 ||
					attribute.type == VertexAttribute::Type::VEC4)
			) {
				vertexData.push_back(vertices[i].x);
				vertexData.push_back(vertices[i].y);
				vertexData.push_back(vertices[i].z);

				if (attribute.type == VertexAttribute::Type::VEC4) {
					vertexData.push_back(1.0f);
				}
			}

			else if (attribute.name == "normal" && (
					attribute.type == VertexAttribute::Type::VEC3 ||
					attribute.type == VertexAttribute::Type::VEC4)
			) {
				if (normals.size() != vertices.size()) {
					Godot::print_error(
						"Shader requires attribute 'normal' not present in the mesh",
						"void Geometry::createVertexBuffer" , "geometry.cpp", 0
					); continue;
				}

				vertexData.push_back(normals[i].x);
				vertexData.push_back(normals[i].y);
				vertexData.push_back(normals[i].z);

				if (attribute.type == VertexAttribute::Type::VEC4) {
					vertexData.push_back(0.0f);
				}
			}

			else if (attribute.name == "tangent" && (
					attribute.type == VertexAttribute::Type::VEC3 ||
					attribute.type == VertexAttribute::Type::VEC4)
			) {
				if (tangents.size() != vertices.size()) {
					Godot::print_error(
						"Shader requires attribute 'tangent' not present in the mesh",
						"void Geometry::createVertexBuffer" , "geometry.cpp", 0
					); continue;
				}

				vertexData.push_back(tangents[i].x);
				vertexData.push_back(tangents[i].y);
				vertexData.push_back(tangents[i].z);

				if (attribute.type == VertexAttribute::Type::VEC4) {
					vertexData.push_back(tangents[i].w);
				}
			}

			else if (attribute.name == "uv" && attribute.type == VertexAttribute::Type::VEC2) {
				if (uvs.size() != vertices.size()) {
					Godot::print_error(
						"Shader requires attribute 'uv' not present in the mesh",
						"void Geometry::createVertexBuffer" , "geometry.cpp", 0
					); continue;
				}

				vertexData.push_back(uvs[i].x);
				vertexData.push_back(uvs[i].y);
			}

			else {
				Godot::print_error(
					"Attribute '" + String(attribute.name.c_str()) + "' not provided.",
					"void Geometry::createVertexBuffer", "geometry.cpp", 0
				);
			}
		}
	}

	if (vertexBuffer) {
		vk::Device device = VKLib::getDevice();
		device.destroyBuffer(vertexBuffer);
		device.freeMemory(vertexBufferMem);
	}

	size_t size = sizeof(vertexData[0]) * vertexData.size();
	vertexBuffer = createBuffer(vk::BufferUsageFlagBits::eVertexBuffer, size, vertexBufferMem);
	fillBufferMemory(vertexBufferMem, size, vertexData.data());
}

void Geometry::createIndexBuffer() {
	if (indexBuffer) {
		vk::Device device = VKLib::getDevice();
		device.destroyBuffer(indexBuffer);
		device.freeMemory(indexBufferMem);
	}

	size_t size = sizeof(indices[0]) * indices.size();
	indexBuffer = createBuffer(vk::BufferUsageFlagBits::eIndexBuffer, size, indexBufferMem);
	fillBufferMemory(indexBufferMem, size, indices.data());
}

void Geometry::createUniformBuffer() {
	if (material == nullptr) {
		Godot::print_error(
			"Attempted to create uniform buffer but material was not set yet",
			"void::Geometry::createUniformBuffer", "geometry.cpp", 0
		); return;
	}

	vk::Device device = VKLib::getDevice();

	for (auto &pair : uniformBuffers) {
		UniformBufferData &ubo = pair.second;
		device.destroyBuffer(ubo.buffer);
		device.freeMemory(ubo.memory);
	}
	uniformBuffers.clear();

	for (const auto &uniform : material->getUniforms()) {
		UniformBufferData ubo;
		ubo.size = uniform.size;
		ubo.buffer = createBuffer(
			vk::BufferUsageFlagBits::eUniformBuffer,
			ubo.size,
			ubo.memory
		);

		uniformBuffers[uniform.binding] = ubo;
	}
}

void Geometry::createDescriptorSet() {
	if (material == nullptr) {
		Godot::print_error(
			"Attempted to create descriptor set but material was not set yet",
			"void::Geometry::createUniformBuffer", "geometry.cpp", 0
		); return;
	}

	vk::DescriptorSetLayout layout    = material->getDescriptorSetLayout();
	vk::DescriptorPool descriptorPool = material->getDescriptorPool();

	if (!layout || !descriptorPool) {
		Godot::print_error(
			"This geometry's material did not have a DescriptorSetLayout or a DescriptorPool",
			"void Geometry::createDescriptorSet", "geometry.cpp", 0
		); return;
	}

	vk::DescriptorSetAllocateInfo allocInfo;
	allocInfo.descriptorPool = descriptorPool;
	allocInfo.descriptorSetCount = 1;
	allocInfo.pSetLayouts = &layout;

	auto device = VKLib::getDevice();

	/*if (descriptorSet && poolFrom) {
		Godot::print("Material: free descriptor set before allocating a new one");
		device.freeDescriptorSets(poolFrom, { descriptorSet });
	}*/

	poolFrom = descriptorPool;
	descriptorSet = device.allocateDescriptorSets(allocInfo)[0];
	updateDescriptorSets();
}

void Geometry::updateDescriptorSets() {
	if (material == nullptr) {
		Godot::print_error(
			"Attempted to update descriptor set but material was not set yet",
			"void::Geometry::createUniformBuffer", "geometry.cpp", 0
		); return;
	}

	auto device = VKLib::getDevice();
	device.waitIdle();

	for (const auto& ubo : material->getUniforms()) {
		vk::DescriptorBufferInfo bufferInfo;
		bufferInfo.buffer = uniformBuffers[ubo.binding].buffer;
		bufferInfo.offset = 0;
		bufferInfo.range  = VK_WHOLE_SIZE;

		vk::WriteDescriptorSet descriptorWrite;
		descriptorWrite.dstSet = descriptorSet;
		descriptorWrite.dstBinding = ubo.binding;
		descriptorWrite.dstArrayElement = 0;
		descriptorWrite.descriptorType = vk::DescriptorType::eUniformBuffer;
		descriptorWrite.descriptorCount = 1;
		descriptorWrite.pBufferInfo = &bufferInfo;

		device.updateDescriptorSets({ descriptorWrite }, {});
	}

	for (const auto& sampler : material->getSamplers()) {
		vk::DescriptorImageInfo imageInfo;
		imageInfo.imageLayout = vk::ImageLayout::eShaderReadOnlyOptimal;
		imageInfo.imageView   = sampler.texture.getImageView();
		imageInfo.sampler     = sampler.texture.getSampler();

		vk::WriteDescriptorSet descriptorWrite;
		descriptorWrite.dstSet = descriptorSet;
		descriptorWrite.dstBinding = sampler.binding;
		descriptorWrite.dstArrayElement = 0;
		descriptorWrite.descriptorType = vk::DescriptorType::eCombinedImageSampler;
		descriptorWrite.descriptorCount = 1;
		descriptorWrite.pImageInfo = &imageInfo;

		device.updateDescriptorSets({ descriptorWrite }, {});
	}
}

void Geometry::recordDrawingCommands(vk::CommandBuffer cmd) {
	if (material == nullptr) return;

	cmd.bindPipeline(vk::PipelineBindPoint::eGraphics, material->getPipeline());
	cmd.bindVertexBuffers(0, { vertexBuffer }, { 0 });
	cmd.bindDescriptorSets(
		vk::PipelineBindPoint::eGraphics,
		material->getPipelineLayout(),
		0, { descriptorSet }, {}
	);

	if (indexed) {
		cmd.bindIndexBuffer(indexBuffer, 0, vk::IndexType::eUint32);
		cmd.drawIndexed(static_cast<uint32_t>(indices.size()), 1, 0, 0, 0);
	} else {
		cmd.draw(static_cast<uint32_t>(vertices.size()), 1, 0, 0);
	}
}

void Geometry::updateUniformBuffer(DefaultUniforms u) {
	if (material == nullptr) return;

	glm::mat4 modelView = u.view * modelMatrix;
	glm::mat4 modelViewProjection = u.cameraMatrix * modelMatrix;
	glm::mat3 normalMatrix = glm::inverseTranspose(glm::mat3(modelView));

	glm::mat4 modelInverse = glm::inverse(modelMatrix);
	glm::mat4 modelViewInverse = glm::inverse(modelView);
	glm::mat4 modelViewProjectionInverse = glm::inverse(modelViewProjection);

	vk::Device device = VKLib::getDevice();
	for (const auto &ubo : material->getUniforms()) {
		UniformBufferData &uniform = uniformBuffers[ubo.binding];

		uint8_t *data;
		data = reinterpret_cast<uint8_t*>(device.mapMemory(uniform.memory, 0, uniform.size));

		for (const auto &member : ubo.members) {
			if (member.name == "delta" && member.type == UBOMember::Type::FLOAT) {
				std::memcpy(data + member.offset, &u.delta, member.size);

			} else if (member.name == "model" && member.type == UBOMember::Type::MAT4) {
				std::memcpy(data + member.offset, &modelMatrix, member.size);

			} else if (member.name == "view" && member.type == UBOMember::Type::MAT4) {
				std::memcpy(data + member.offset, &u.view, member.size);

			} else if (member.name == "projection" && member.type == UBOMember::Type::MAT4) {
				std::memcpy(data + member.offset, &u.projection, member.size);

			} else if (member.name == "modelView" && member.type == UBOMember::Type::MAT4) {
				std::memcpy(data + member.offset, &modelView, member.size);

			} else if (member.name == "modelViewProjection" && member.type == UBOMember::Type::MAT4) {
				std::memcpy(data + member.offset, &modelViewProjection, member.size);

			} else if (member.name == "cameraMatrix" && member.type == UBOMember::Type::MAT4) {
				std::memcpy(data + member.offset, &u.cameraMatrix, member.size);

			} else if (member.name == "normalMatrix" && member.type == UBOMember::Type::MAT3) {
				std::memcpy(data + member.offset, &normalMatrix, member.size);

			} else if (member.name == "modelInverse" && member.type == UBOMember::Type::MAT4) {
				std::memcpy(data + member.offset, &modelInverse, member.size);

			} else if (member.name == "viewInverse" && member.type == UBOMember::Type::MAT4) {
				std::memcpy(data + member.offset, &u.viewInverse, member.size);

			} else if (member.name == "projectionInverse" && member.type == UBOMember::Type::MAT4) {
				std::memcpy(data + member.offset, &u.projectionInverse, member.size);

			} else if (member.name == "modelViewInverse" && member.type == UBOMember::Type::MAT4) {
				std::memcpy(data + member.offset, &modelViewInverse, member.size);

			} else if (member.name == "modelViewProjectionInverse" && member.type == UBOMember::Type::MAT4) {
				std::memcpy(data + member.offset, &modelViewProjectionInverse, member.size);
			}
		}

		device.unmapMemory(uniform.memory);
	}

	Array exportedUniforms = material->getExportedUniforms();

	int current_binding = -1;
	uint8_t *data;
	for (int i = 0; i < exportedUniforms.size(); i++) {
		if (exportedUniforms[i].get_type() != Variant::Type::DICTIONARY) continue;

		Dictionary d = exportedUniforms[i];
		if (!d.has("value")) {
			String type = d["type"];
			if (type == "sampler2D") continue;

			Godot::print(String("Exported uniform '") + d["name"] + "' has no value, fixing");
			if (type == "int" || type == "uint" || type == "float") d["value"] = 1;
			else if (type == "ivec2" || type == "uvec2" || type == "vec2") d["value"] = Vector2(1.0f, 1.0f);
			else if (type == "ivec3" || type == "uvec3" || type == "vec3") d["value"] = Vector3(1.0f, 1.0f, 1.0f);
			else if (type == "color") d["value"] = Color(1.0f, 1.0f, 1.0f, 1.0f);
			else {
				Godot::print_error(
					"Failed to fix uniform: invalid uniform type " + type,
					"void Geometry::updateUniformBuffer", "geometry.cpp", 0
				); continue;
			}
		}

		uint32_t size = d["size"];
		uint32_t offset = d["offset"];
		uint32_t binding = d["binding"];

		if (static_cast<int>(binding) != current_binding) {
			if (current_binding != -1) {
				device.unmapMemory(uniformBuffers[current_binding].memory);
			}

			current_binding = binding;

			UniformBufferData &uniform = uniformBuffers[current_binding];
			data = reinterpret_cast<uint8_t*>(device.mapMemory(uniform.memory, 0, uniform.size));
		}

		if (d["type"] == "int") {
			int value = d["value"];
			std::memcpy(data + offset, &value, size);

		} else if (d["type"] == "uint") {
			uint32_t value = d["value"];
			std::memcpy(data + offset, &value, size);

		} else if (d["type"] == "float") {
			float value = d["value"];
			std::memcpy(data + + offset, &value, size);

		} else if (d["type"] == "ivec2") {
			Vector2 _value = d["value"];
			ivec2 value = ivec2(_value.x, _value.y);
			std::memcpy(data + offset, &value, size);

		} else if (d["type"] == "ivec3") {
			Vector3 _value = d["value"];
			ivec3 value = ivec3(_value.x, _value.y, _value.z);
			std::memcpy(data + offset, &value, size);

		} else if (d["type"] == "uvec2") {
			Vector2 _value = d["value"];
			uvec2 value = uvec2(_value.x, _value.y);
			std::memcpy(data + offset, &value, size);

		} else if (d["type"] == "uvec3") {
			Vector3 _value = d["value"];
			uvec3 value = uvec3(_value.x, _value.y, _value.z);
			std::memcpy(data + offset, &value, size);

		} else if (d["type"] == "vec2") {
			Vector2 _value = d["value"];
			vec2 value = vec2(_value.x, _value.y);
			std::memcpy(data + offset, &value, size);

		} else if (d["type"] == "vec3") {
			Vector3 _value = d["value"];
			vec3 value = vec3(_value.x, _value.y, _value.z);
			std::memcpy(data + offset, &value, size);

		} else if (d["type"] == "color") {
			Color _value = d["value"];
			vec4 value = vec4(_value.r, _value.g, _value.b, _value.a);
			std::memcpy(data + offset, &value, size);
		}
	}

	if (current_binding != -1) {
		device.unmapMemory(uniformBuffers[current_binding].memory);
	}
}

void Geometry::cleanup() {
	vk::Device device = VKLib::getDevice();

	device.freeDescriptorSets(poolFrom/*material->getDescriptorPool()*/, { descriptorSet });

	device.destroyBuffer(indexBuffer);
	device.destroyBuffer(vertexBuffer);

	device.freeMemory(indexBufferMem);
	device.freeMemory(vertexBufferMem);

	for (auto &pair : uniformBuffers) {
		UniformBufferData &ubo = pair.second;
		device.destroyBuffer(ubo.buffer);
		device.freeMemory(ubo.memory);
	}
	uniformBuffers.clear();
}

// ##################################################
