#pragma once

#include <glm/glm.hpp>

using namespace glm;

class Camera {
public:
	Camera();
	Camera(float aspect);

	vec3 position;
	vec3 target;
	const vec3 up = vec3(0.0f, 1.0f, 0.0f);

	void setDirection(vec3);
	vec3 getDirection() const;

	void setProjectionParams(
		float fov, float aspect,
		float near, float far
	);

	mat4 getViewMatrix() const;
	mat4 getProjectionMatrix() const;
	mat4 getCameraMatrix() const;

	void rotate(float angleX, float angleY);
	void orbit(float angleX, float angleY);

protected:
	mat4 projection;
};
