#pragma once

#include <vulkan/vulkan.hpp>

uint32_t findMemoryType(uint32_t type_filter, vk::MemoryPropertyFlags);

vk::Buffer createBuffer(
	vk::BufferUsageFlags,
	size_t,
	vk::DeviceMemory&,
	vk::SharingMode sharingMode = vk::SharingMode::eExclusive,
	vk::MemoryPropertyFlags memPropertyFlags =
		vk::MemoryPropertyFlagBits::eHostVisible |
		vk::MemoryPropertyFlagBits::eHostCoherent
);

void fillBufferMemory(vk::DeviceMemory, size_t, const void*);

void copyBuffer(vk::Buffer source, vk::Buffer destination, vk::DeviceSize);

vk::Image createImage(
	uint32_t width, uint32_t height,
	vk::Format,
	vk::ImageTiling,
	vk::ImageUsageFlags,
	vk::MemoryPropertyFlags,
	vk::DeviceMemory&
);

vk::ImageView createImageView(vk::Image, vk::Format);

void transitionImageLayout(
	vk::Image, vk::Format,
	vk::ImageLayout oldLayout, vk::ImageLayout newLayout,
	vk::ImageAspectFlags aspect = vk::ImageAspectFlagBits::eColor
);

void copyBufferToImage(vk::Buffer, vk::Image, uint32_t width, uint32_t height);
