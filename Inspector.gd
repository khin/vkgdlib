extends Container

################################################################################

export(PackedScene) var property_edit
export(NodePath) var _manager
export(NodePath) var _geometry_list
export(NodePath) var _material_list

################################################################################

onready var manager = get_node(_manager)
onready var geometry_list = get_node(_geometry_list)
onready var material_list = get_node(_material_list)

var selected_geometry
var selected_geometry_name

################################################################################

func geometry_selected(index):
	selected_geometry = geometry_list.get_item_metadata(index)
	selected_geometry_name = geometry_list.get_item_text(index)
	
	clear_properties()
	add_properties(selected_geometry)

func clear_properties():
	for item in $Scroll/List.get_children():
		item.queue_free()

func add_properties(geometry):
	var inspector = $Scroll/List
	
	var edit = property_edit.instance()
	edit.set_property("Position", "vec3", selected_geometry.position)
	edit.connect("property_changed", self, "set_geometry_property")
	inspector.add_child(edit)
	
	edit = property_edit.instance()
	edit.set_property("Rotation", "vec3", selected_geometry.rotation)
	edit.connect("property_changed", self, "set_geometry_property")
	inspector.add_child(edit)
	
	edit = property_edit.instance()
	edit.set_property("Scale", "vec3", selected_geometry.scale)
	edit.connect("property_changed", self, "set_geometry_property")
	inspector.add_child(edit)
	
	edit = property_edit.instance()
	edit.set_property("Material", "list", get_items_from_list(material_list))
	edit.set_list_selection(selected_geometry.get_material_name())
	edit.connect("property_changed", self, "set_geometry_property")
	inspector.add_child(edit)

func get_items_from_list(list):
	var items = PoolStringArray()
	for i in range(list.get_item_count()):
		items.append(list.get_item_text(i))
	return items

func set_geometry_property(property_name, value):
	match property_name:
		"Position": selected_geometry.position = value
		"Rotation": selected_geometry.rotation = value
		"Scale": selected_geometry.scale = value
		"Material": manager.set_geometry_material(selected_geometry_name, value)
		_: pass

func update_material_options(material_dict = null):
	if selected_geometry == null: return
	
	if $Scroll/List.get_child_count() < 4: return
	var material_property = $Scroll/List.get_child(3)
	material_property.set_property("Material", "list", get_items_from_list(material_list))
	

################################################################################
