#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UBO {
	mat4 model;
	mat4 modelInverse;
	mat4 viewInverse;
	mat4 modelViewProjection;

	vec3 lightPosition;
};

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normal;
layout(location = 2) in vec4 tangent;
layout(location = 3) in vec2 uv;

layout(location = 0) out vec2 _uv;
layout(location = 1) out vec3 _viewDirection; // all in world coordinates
layout(location = 2) out vec3 _normal;
layout(location = 3) out vec3 _tangent;
layout(location = 4) out vec3 _binormal;
layout(location = 5) out vec3 _lightDirection;
layout(location = 6) out vec3 _halfAngle;

void main() {
	gl_Position = modelViewProjection * position;

	vec3 _position = (model * position).xyz;
	vec3 _cameraPosition = (transpose(viewInverse))[3].xyz;

	mat4 modelIT = transpose(modelInverse);

	_uv = uv;
	_viewDirection = normalize(_cameraPosition - _position);
	_normal = (modelIT * normal).xyz;
	_tangent = (modelIT * vec4(tangent.xyz, 1.0)).xyz;
	_binormal = cross(normal.xyz, tangent.xyz) * tangent.w;
	_lightDirection = normalize(lightPosition - _position);
	_halfAngle = normalize(_viewDirection + _lightDirection);
}
