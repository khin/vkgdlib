#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UBO {
	mat4 modelViewProjection;
	float transparency;
};

layout(location = 0) in vec3 position;
layout(location = 1) in vec3 normal;
layout(location = 0) out vec4 color;

out gl_PerVertex {
	vec4 gl_Position;
};

void main() {
	gl_Position = modelViewProjection * vec4(position, 1.0);
	color = vec4(normal, transparency);
}
