#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 1) uniform UBOf {
	vec4 lightColor;
	vec4 ambient;
	vec4 specular;
	float shininess;

	float texScale;
	float bumpy;
};

layout(binding = 2) uniform sampler2D diffuseTexture;
layout(binding = 3) uniform sampler2D normalTexture;

layout(location = 0) in vec2 _uv;
layout(location = 1) in vec3 _viewDirection; // all in world coordinates
layout(location = 2) in vec3 _normal;
layout(location = 3) in vec3 _tangent;
layout(location = 4) in vec3 _binormal;
layout(location = 5) in vec3 _lightDirection;
layout(location = 6) in vec3 _halfAngle;

layout(location = 0) out vec4 fragColor;

vec4 ambientReflection(vec4 surfaceColor, vec4 lightColor) {
	return surfaceColor * lightColor;
}

vec4 diffuseReflection(
	vec4 Kd,
	vec3 surfaceNormal,
	vec4 lightColor,
	vec3 lightDirection
) {
	float diffuseFactor = max(0, dot(lightDirection, surfaceNormal));
	return lightColor * Kd * diffuseFactor;
}

vec4 specularReflection(
	vec4 surfaceColor,
	float surfaceShininess,
	vec3 surfaceNormal,
	vec4 lightColor,
	vec3 halfAngle
) {
	float specularFactor = pow(max(0, dot(halfAngle, surfaceNormal)), surfaceShininess);
	return lightColor * surfaceColor * specularFactor;
}

vec4 phongReflection(
	vec4 ambientSurfaceColor,
	vec4 ambientLightColor,
	vec4 diffuseSurfaceColor,
	vec4 specularSurfaceColor,
	float surfaceShininess,
	vec3 surfaceNormal,
	vec3 halfAngle,
	vec3 lightDirection,
	vec4 lightColor
) {
	vec4 ambient = ambientReflection(ambientSurfaceColor, ambientLightColor);
	vec4 diffuse = diffuseReflection(diffuseSurfaceColor, surfaceNormal, lightColor, lightDirection);
	vec4 specular;
	if (dot(lightDirection, surfaceNormal) <= 0) {
		specular = vec4(0);
	} else {
		specular = specularReflection(
			specularSurfaceColor,
			surfaceShininess,
			surfaceNormal,
			lightColor,
			halfAngle
		);
	}

	return ambient + diffuse + specular;
}

vec3 expand(vec3 v) {
	return (v - 0.5) * 2.0;
}

void main() {
	vec4 diffuse = texture(diffuseTexture, _uv * texScale);

	vec3 normal = normalize(_normal);
	vec3 tangent = normalize(_tangent);
	vec3 binormal = normalize(_binormal);

	vec3 sampled = texture(normalTexture, _uv * texScale).xyz;

	vec3 Nb = sampled.x * binormal + sampled.y * tangent + sampled.z * normal;
	Nb = normalize(Nb) * bumpy;

	fragColor = phongReflection(
		diffuse, // surface ambient, same as surface diffuse
		ambient, // light ambient
		diffuse, // surface diffuse
		specular,
		shininess,
		Nb,
		normalize(_halfAngle),
		normalize(_lightDirection),
		vec4(lightColor.rgb, 1)
	);
	fragColor.a = 1;
}
