#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 1) uniform sampler2D texture_sampler;

layout(location = 0) in vec4 diffuse_ambient;
layout(location = 1) in vec4 specular;
layout(location = 2) in vec2 vertex_uv;

layout(location = 0) out vec4 color;

void main() {
	vec4 texture_color = texture(texture_sampler, vertex_uv * 5.0);
	color = diffuse_ambient * texture_color + specular;
}
