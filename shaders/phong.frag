#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 1) uniform UBOf {
	vec4 lightColor;
	vec4 ambient;
	vec4 specular;

	vec4 diffuse;
	float shininess;
};

layout(location = 0) in vec3 _normal;
layout(location = 1) in vec3 _viewDirection;
layout(location = 2) in vec3 _lightDirection;
layout(location = 3) in vec3 _halfAngle;

layout(location = 0) out vec4 color;

vec4 ambientReflection(vec4 surfaceColor, vec4 lightColor) {
	return surfaceColor * lightColor;
}

vec4 diffuseReflection(
	vec4 Kd,
	vec3 surfaceNormal,
	vec4 lightColor,
	vec3 lightDirection
) {
	float diffuseFactor = max(0, dot(lightDirection, surfaceNormal));
	return lightColor * Kd * diffuseFactor;
}

vec4 specularReflection(
	vec4 surfaceColor,
	float surfaceShininess,
	vec3 surfaceNormal,
	vec4 lightColor,
	vec3 halfAngle
) {
	float specularFactor = pow(max(0, dot(halfAngle, surfaceNormal)), surfaceShininess);
	return lightColor * surfaceColor * specularFactor;
}

vec4 phongReflection(
	vec4 ambientSurfaceColor,
	vec4 ambientLightColor,
	vec4 diffuseSurfaceColor,
	vec4 specularSurfaceColor,
	float surfaceShininess,
	vec3 surfaceNormal,
	vec3 halfAngle,
	vec3 lightDirection,
	vec4 lightColor
) {
	vec4 ambient = ambientReflection(ambientSurfaceColor, ambientLightColor);
	vec4 diffuse = diffuseReflection(diffuseSurfaceColor, surfaceNormal, lightColor, lightDirection);
	vec4 specular;
	if (dot(lightDirection, surfaceNormal) <= 0) {
		specular = vec4(0);
	} else {
		specular = specularReflection(
			specularSurfaceColor,
			surfaceShininess,
			surfaceNormal,
			lightColor,
			halfAngle
		);
	}

	return ambient + diffuse + specular;
}

void main() {
	color = phongReflection(
		diffuse, // surface ambient, same as surface diffuse
		ambient, // light ambient
		diffuse, // surface diffuse
		specular,
		shininess,
		normalize(_normal),
		normalize(_halfAngle),
		normalize(_lightDirection),
		vec4(lightColor.rgb, 0)
	);
}
