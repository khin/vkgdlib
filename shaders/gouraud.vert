#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UBO {
	// Transform matrices
	mat4 view;
	mat4 modelView;
	mat4 modelViewProjection;
	mat3 normalMatrix;

	// Material parameters:
	vec3 directional_light;
	vec4 light_color;
	vec4 light_ambient;

	vec4 diffuse;
	vec4 specular;
	float shininess;
} ubo;

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normal;
layout(location = 2) in vec2 uv;

layout(location = 0) out vec4 diffuse_ambient;
layout(location = 1) out vec4 specular;
layout(location = 2) out vec2 vertex_uv;

void main() {
	vertex_uv = uv;
	gl_Position = ubo.modelViewProjection * position;

	vec3 pos = (ubo.modelView * position).xyz; // position relative to the camera
	vec3 N = ubo.normalMatrix * normal.xyz;
	vec3 E = normalize(-pos);
	vec3 L = -normalize((ubo.view * vec4(ubo.directional_light, 1.0)).xyz);
	vec3 H = normalize(E + L);

	float diff = max(0, dot(N, L));
	float spec = pow(max(0, dot(N, H) ), ubo.shininess);

	if (diff <= 0.0) {
		spec = 0.0;
	}

	vec4 ambient_color = ubo.diffuse * ubo.light_ambient;
	vec4 diffuse_color = ubo.diffuse * diff * ubo.light_color;
	vec4 specular_color = ubo.specular * ubo.light_color * spec;

	diffuse_ambient = diffuse_color + ambient_color;
	specular = specular_color;
}
