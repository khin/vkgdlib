#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UBO {
	mat4 model;
	mat4 modelInverse;
	mat4 viewInverse;
	mat4 modelViewProjection;

	vec3 lightPosition;
};

layout(location = 0) in vec4 position;
layout(location = 1) in vec4 normal;

layout(location = 0) out vec3 _normal;
layout(location = 1) out vec3 _viewDirection;
layout(location = 2) out vec3 _lightDirection;
layout(location = 3) out vec3 _halfAngle;

void main() {
	gl_Position = modelViewProjection * position;

	vec3 _position = (model * position).xyz;
	vec3 _cameraPosition = (transpose(viewInverse))[3].xyz;

	mat4 modelIT = transpose(modelInverse);

	_viewDirection = normalize(_cameraPosition - _position);
	_normal = (modelIT * normal).xyz;
	_lightDirection = normalize(lightPosition - _position);
	_halfAngle = normalize(_viewDirection + _lightDirection);
}
