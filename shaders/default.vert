#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UniformBufferObject {
	mat4 modelViewProjection;
} ubo;

layout(location = 0) in vec4 position;

void main() {
	gl_Position = ubo.modelViewProjection * position;
}
