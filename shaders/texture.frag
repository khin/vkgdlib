#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 1) uniform sampler2D color_texture;

layout(location = 0) in vec2 uv;
layout(location = 0) out vec4 color;

void main() {
	color = texture(color_texture, uv);
}
