#version 450
#extension GL_ARB_separate_shader_objects : enable

layout(binding = 0) uniform UniformBufferObject {
	mat4 modelViewProjection;
} ubo;

layout(location = 0) in vec4 position;
layout(location = 1) in vec2 uv;

layout(location = 0) out vec2 _uv;

void main() {
	gl_Position = ubo.modelViewProjection * position;
	_uv = uv;
}
