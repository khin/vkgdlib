extends Node

################################################################################

export(NodePath) var _material_manager
export(NodePath) var _material_params
export(Texture) var default_texture

################################################################################

onready var material_manager = get_node(_material_manager)
onready var uniforms_editor = get_node(_material_params)
onready var vklib = get_node("../VulkanWindow")

var ShaderClass = preload("res://scripts/shader.gdns")
var MaterialClass = preload("res://scripts/material.gdns")
var GeometryClass = preload("res://scripts/geometry.gdns")

var materials = Dictionary()
var objects = Dictionary()

################################################################################

func _ready():
	material_manager.add_material("Default material", "res://shaders/default.vert", "res://shaders/default.frag")
	material_manager.on_material_executed()

func setup_material(material_dict):
	material_manager.clear_compilation_status()
	
	var vertex_shader = load_shader(material_dict["vert"])
	var fragment_shader = load_shader(material_dict["frag"])
	
	if not vertex_shader or not fragment_shader:
		return
	
	material_manager.set_compilation_status("Shader compilation successful.")
	
	var material_name = material_dict["name"]
	var uniform_buffer = []
	
	var material
	if materials.has(material_name):
		material = materials[material_name]
		uniform_buffer = material.get_exported_uniforms().duplicate()
	else:
		material = MaterialClass.new()
	
	material.set_shader(vertex_shader)
	material.set_shader(fragment_shader)
	
	var image = default_texture.get_data()
	material.set_default_texture_image(image)
	
	materials[material_name] = material
	material.setup(material_name)
	
	var new_uniforms = material.get_exported_uniforms()
	for uniform in uniform_buffer:
		for new_uniform in new_uniforms:
			if new_uniform["name"] == uniform["name"] and new_uniform["type"] == uniform["type"]:
				if uniform.has("value"):
					new_uniform["value"] = uniform["value"]
				if uniform.has("path"):
					new_uniform["path"] = uniform["path"]
					new_uniform["value"] = load(uniform["path"])
	
	vertex_shader.destroy_module()
	fragment_shader.destroy_module()
	on_material_selected(0)
	
	for geometry in objects.values():
		if geometry.get_material_name() == material.get_name():
			geometry.set_material(material)
	
	vklib.clear_command_buffer()
	for geometry in objects.values():
		geometry.create_descriptor_set()
	vklib.create_command_buffer()

func load_shader(filepath):
	var shader = ShaderClass.new()
	shader.load(ProjectSettings.globalize_path(filepath))
	
	var compiled = shader.compile()
	if not compiled:
		var message = shader.get_compilation_status_string()
		material_manager.append_compilation_status(message)
		return false
	
	shader.create_module()
	return shader

##################################################

func add_geometry(geometry_name, mesh):
	if objects.keys().has(geometry_name):
		printerr("There already is a geometry called '%s'. Denied." % geometry_name)
		return false
	
	var geometry = GeometryClass.new()
	geometry.set_array_mesh(mesh)
	
	objects[geometry_name] = geometry
	vklib.add_geometry(geometry)
	set_geometry_material(geometry_name, "Default material")
	
	return true

func get_geometry(geometry_name):
	if not objects.keys().has(geometry_name):
		printerr("Object '%s' not found." % geometry_name)
		return false
	return objects[geometry_name]

func set_geometry_material(geometry_name, material_name):
	if not material_name in materials.keys(): return
	if not geometry_name in objects.keys(): return
	
	vklib.clear_command_buffer()
	
	objects[geometry_name].set_material(materials[material_name])
	objects[geometry_name].create_descriptor_set()
	
	vklib.create_command_buffer()

################################################################################

func _process(delta):
	var uniforms = uniforms_editor.get_uniforms()
	if uniforms.size() == 0: return
	
	var material = get_selected_material()
	if not material: return
	
	material.set_exported_uniforms_values(uniforms)

func on_material_selected(id):
	uniforms_editor.clear_uniforms()
	
	var material = get_selected_material()
	if not material: return
	
	uniforms_editor.parse_material_uniforms(material)

func get_selected_material():
	var material_name = material_manager.get_selected_material_name()
	if not material_name in materials.keys():
		printerr("Material '%s' not found" % material_name)
		return false
	return materials[material_name]

func on_texture_selected(path):
	var texLoader = get_node("../LoadTextureFile")
	if not texLoader.has_meta("sampler"):
		return

	var sampler = texLoader.get_meta("sampler")
	if not sampler.has("name") or not sampler.has("value"):
		return

	var material = get_selected_material()
	if not material: return

	vklib.clear_command_buffer()

	material.set_texture(sampler["name"], sampler["value"])
	for geometry in objects.values():
		geometry.create_descriptor_set()

	vklib.create_command_buffer()

func _exit_tree():
	vklib.clear_command_buffer()
	
	for object in objects.values():
		object.cleanup()
		
	for material in materials.values():
		material.cleanup()

################################################################################
