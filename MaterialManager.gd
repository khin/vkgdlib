extends Container

################################################################################

export(NodePath) var _vertex_edit
export(NodePath) var _fragment_edit
export(NodePath) var _compilation_status

signal material_executed(material_dict)

################################################################################

onready var vert_edit = get_node(_vertex_edit)
onready var frag_edit = get_node(_fragment_edit)
onready var status = get_node(_compilation_status)

var materials = {}

################################################################################

func add_material(material_name, vert_filepath, frag_filepath):
	if materials.keys().has(material_name):
		return
	
	var m = {}
	m["name"] = material_name
	m["vert"] = vert_filepath
	m["frag"] = frag_filepath
	materials[material_name] = m
	
	$MaterialList.add_item(material_name)
	
	var id = $MaterialList.get_item_count() - 1
	$MaterialList.select(id)
	$MaterialList.emit_signal("item_selected", id)

func on_material_selected(id):
	var material_name = $MaterialList.get_item_text(id)
	if not material_name in materials.keys():
		printerr("Selected material not found: %s (id: %d)" % [material_name, id])
		return
	
	var m = materials[material_name]
	display_file(m["vert"], vert_edit)
	display_file(m["frag"], frag_edit)
	clear_compilation_status()

func get_selected_material_name():
	var selected_items = $MaterialList.get_selected_items()
	if selected_items.size() == 0:
		printerr("No material selected")
		return ""
	
	var material_name = $MaterialList.get_item_text(selected_items[0])
	return material_name

func on_material_executed():
	var material_name = get_selected_material_name()
	if not material_name in materials.keys():
		printerr("Material not found: %s" % material_name)
		return
	
	var m = materials[material_name]
	save_file(vert_edit, m["vert"])
	save_file(frag_edit, m["frag"])
	emit_signal("material_executed", m)

################################################################################

func display_file(path, editor):
	var file = File.new()
	if not file.file_exists(path):
		printerr("File doesn't exist: %s" % path)
		return
	
	file.open(path, File.READ)
	if not file.is_open():
		printerr("Cannot open file: %s" % path)
		return
	
	editor.text = file.get_as_text()
	file.close()

func save_file(editor, path):
	var file = File.new()
	if not file.file_exists(path):
		printerr("File doesn't exist: %s" % path)
		return
	
	file.open(path, File.WRITE)
	if not file.is_open():
		printerr("Cannot open file: %s" % path)
		return
	
	file.store_string(editor.text)
	file.close()

func clear_compilation_status():
	status.text = ""

func set_compilation_status(message):
	status.text = message + "\n"

func append_compilation_status(message):
	status.text += message + "\n"

################################################################################
