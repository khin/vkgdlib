# VKLib

## Abstract
In order to learn shader programming, students often have to create the entire host application to manually load 3-dimensional meshes and images, and deal with specific graphic API details. This process can easily distract the student, deviating attention from the actual shader programming. There are tools to assist these tasks, such as NVidia's FX Composer - [which has been discontinued since 2008][FX Composer 2.5 release notes] - and some online resources, like [Shadertoy][shadertoy] and [Shdr][shdr], but those are often difficult to use, [have a limited feature set][shdr help], and/or lack documentation to get beginners started.

In this work we built an easy-to-use desktop tool which provides the full environment required for shader programming from beginner to advanced, with a user-friendly interface which allows students to effortlessly load 3D models and images and modify variables and textures while the program is running, applying the changes in real time.

**Keywords:** shader programming, GLSL, vulkan, Godot Engine

---
### Technologies
The application is built using C++ and Vulkan as the graphics API. The use of Vulkan is transparent to the end-user - the choice of API was a personal one, exclusively to fulfill my intent of learning the new technology in order to stay on par with what is used in the industry. The GUI is built using Godot Engine 3, which allows the loading of custom libraries through a technology they call GDNative.

#### Godot Engine and GDNative
Godot is an open source game engine built in C++. For this work, the engine is used to deal with mesh and image loading, as well as all user interface configuration and functionality.

**GDNative** allows developers to write libraries in a variety of languages and the engine will load the libraries and expose them to be used in scripting. In this case, all graphics operations are written in C++ and then compiled into a library. Godot loads this library, and the user interface utilizes the functions in the library to control the application via scripting (GDScript, Godot's own scripting language).

Using **Godot Engine** for this work allowed me to create a responsive user interface, as well as handling loading images files to be used as textures. This reduced the amount of work to implement those things, or reduced the number of dependecies for the project, should I have chosen to use external libraries to accomplish those same things.

#### Vulkan
**Vulkan** is [a low-overhead, cross-platform 3D graphics and compute API][Vulkan wiki] created by the Khronos Group, [an industry consortium focused on the creation of open standards, royalty-free APIs for authoring and accelerated playback of dynamic media on a wide variety of platforms and devices][Khronos wiki].

### Assisting libraries used
- **[Vulkan-Hpp][vulkan hpp]**
  - The goal of the Vulkan-Hpp is to provide header only C++ bindings for the Vulkan C API to improve the developers Vulkan experience without introducing CPU runtime cost. It adds features like type safety for enums and bitfields, STL container support, exceptions and simple enumerations.


- **[libshaderc][libshaderc]**
  - A library for compiling shader strings into SPIR-V;
  - Part of Google's [Shaderc][shaderc] tool;
  - Used in this work to compile GLSL source code into SPIR-V.


- **[SPIRV-Cross][spirv-cross]**
  - SPIRV-Cross is a tool designed for parsing and converting SPIR-V to other shader languages;
  - Profides a reflection API to simplify the creation of Vulkan pipeline layouts, used in this work.


- **[OpenGL Mathematics][glm]**
  - OpenGL Mathematics (GLM) is a header only C++ mathematics library for graphics software based on the OpenGL Shading Language (GLSL) specifications;
  - Used in this work to handle mathematical types such as vectors and matrices, and their operations (cross products, matrix multiplication, etc.).


- **[Godot-cpp][godot cpp]**
  - C++ bindings for the Godot script API;
  - Required to use C++ scripts with Godot Engine.

---

### Architecture

#### Classes
< Describe the classes, their responsibilities, methods and everything >

#### Working with Vulkan
< Working with Vulkan was the thing I did the most while programming this work, and that should be the focus of this section, the caveats and workings of everything I used >

---
[FX Composer 2.5 release notes]: https://developer.nvidia.com/sites/default/files/akamai/gamedev/docs/FX_Composer_2.5_README.TXT "FX Composer 2.5 release notes"
[Shadertoy]:https://www.shadertoy.com/ "Shadertoy BETA"
[Shdr]: http://shdr.bkcore.com/ "Shdr"

[Shdr help]: https://github.com/BKcore/Shdr/wiki/Help "Shdr's Help page"
[Shadr commit history]: https://github.com/BKcore/Shdr/commits/master "Shdr commit history"

[Vulkan wiki]: https://en.wikipedia.org/wiki/Vulkan_(API) "Vulkan API wikipedia article"
[Khronos wiki]: https://en.wikipedia.org/wiki/Khronos_Group "Khronos Group wikipedia article"

[GDNative release article]: https://godotengine.org/article/dlscript-here
[GDNative architecture article]: https://godotengine.org/article/look-gdnative-architecture

[Vulkan HPP]: https://github.com/KhronosGroup/Vulkan-Hpp "Vulkan HPP repository"
[libshaderc]: https://github.com/google/shaderc/tree/master/libshaderc "libshaderc repository"
[Shaderc]: https://github.com/google/shaderc "shaderc repository"
[SPIRV-Cross]: https://github.com/KhronosGroup/SPIRV-Cross "SPIRV-Cross repository"
[GLM]: https://glm.g-truc.net "GLM homepage"
[Godot CPP]: https://github.com/GodotNativeTools/godot-cpp "Godot-cpp repository"

[Markdown cheatsheet]: https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet
