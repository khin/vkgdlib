extends WindowDialog

signal create_material(material_name, vertex_shader_path, fragment_shader_path)

func _ready():
	$"Grid/ContainerVertex/Button".connect("pressed", self, "load_shader", [ $"Grid/ContainerVertex/LineEdit", "*.vert" ])
	$"Grid/ContainerFragment/Button".connect("pressed", self, "load_shader", [ $"Grid/ContainerFragment/LineEdit", "*.frag" ])

func clear_fields():
	$"Grid/LineEditName".text = ""
	$"Grid/ContainerVertex/LineEdit".text = ""
	$"Grid/ContainerFragment/LineEdit".text = ""
	$"LoadShaderFileDialog".filename = ""

func load_shader(line_edit, filter):
	if $LoadShaderFileDialog.is_connected("file_selected", $"Grid/ContainerVertex/LineEdit", "set_text"):
		$LoadShaderFileDialog.disconnect("file_selected", $"Grid/ContainerVertex/LineEdit", "set_text")
	
	if $LoadShaderFileDialog.is_connected("file_selected", $"Grid/ContainerFragment/LineEdit", "set_text"):
		$LoadShaderFileDialog.disconnect("file_selected", $"Grid/ContainerFragment/LineEdit", "set_text")
	
	$LoadShaderFileDialog.connect("file_selected", line_edit, "set_text", [], CONNECT_ONESHOT)
	$LoadShaderFileDialog.filters = [ filter ]
	$LoadShaderFileDialog.popup_centered()

func on_create_clicked():
	var name = $"Grid/LineEditName".text
	var vert = $"Grid/ContainerVertex/LineEdit".text
	var frag = $"Grid/ContainerFragment/LineEdit".text
	
	if name.empty() or vert.empty() or frag.empty():
		return
	
	emit_signal("create_material", name, vert, frag)
	hide()
