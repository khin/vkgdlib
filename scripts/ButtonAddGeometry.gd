extends MenuButton

export(NodePath) var _manager

onready var manager = get_node(_manager)
onready var list = get_node("../ObjectList")

func _ready():
	get_popup().connect("index_pressed", self, "create_geometry")
	list.connect("item_selected", self, "geometry_selected")

var counter = 0
func create_geometry(index):
	var type = get_popup().get_item_text(index)
	
	var geometry
	match type:
		"Capsule":  geometry = CapsuleMesh.new()
		"Cube":     geometry = CubeMesh.new()
		"Cylinder": geometry = CylinderMesh.new()
		"Plane":    geometry = PlaneMesh.new()
		"Prism":    geometry = PrismMesh.new()
		"Sphere":   geometry = SphereMesh.new()
		"Load OBJ":
			if not $ObjFileDialog.is_connected("file_selected", self, "loaded_obj"):
				$ObjFileDialog.connect("file_selected", self, "loaded_obj", [], CONNECT_ONESHOT)
			$ObjFileDialog.popup_centered()
			return
		_: return
	
	var geometry_name = type + "_" + String(counter)
	if manager.add_geometry(geometry_name, geometry):
		list.add_item(geometry_name)
		list.set_item_metadata(counter, manager.objects[geometry_name])
		counter += 1

func loaded_obj(path):
	var geometry_name = path.get_file() + "_" + String(counter)
	var geometry = load(path)
	if manager.add_geometry(geometry_name, geometry):
		list.add_item(geometry_name)
		list.set_item_metadata(counter, manager.objects[geometry_name])
		counter += 1

var selected_geometry = null
var geometry_name = ""

export(NodePath) var _inspector
export(PackedScene) var property_edit
onready var inspector = get_node(_inspector)
func geometry_selected(index):
	selected_geometry = list.get_item_metadata(index)
	geometry_name = list.get_item_text(index)
	
	for property in inspector.get_children():
		property.queue_free()
	
	var edit = property_edit.instance()
	edit.set_property("Position", "vec3", selected_geometry.position)
	edit.connect("property_changed", self, "set_geometry_property")
	inspector.add_child(edit)
	
	edit = property_edit.instance()
	edit.set_property("Rotation", "vec3", selected_geometry.rotation)
	edit.connect("property_changed", self, "set_geometry_property")
	inspector.add_child(edit)
	
	edit = property_edit.instance()
	edit.set_property("Scale", "vec3", selected_geometry.scale)
	edit.connect("property_changed", self, "set_geometry_property")
	inspector.add_child(edit)
	
	edit = property_edit.instance()
	edit.set_property("Material", "list", manager.materials.keys())
	edit.set_list_selection(selected_geometry.get_material_name())
	edit.connect("property_changed", self, "set_geometry_property")
	inspector.add_child(edit)

func set_geometry_property(property_name, value):
	match property_name:
		"Position": selected_geometry.position = value
		"Rotation": selected_geometry.rotation = value
		"Scale": selected_geometry.scale = value
		"Material": manager.set_geometry_material(geometry_name, value)
		_: pass
