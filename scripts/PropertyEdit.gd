extends Control

signal property_changed(property_name, value)

var property_name
var property_type

export var LIMITS = 10000

func set_property(name, type, value = null):
	property_name = name
	property_type = type
	
	$PropertyLabel.text = property_name
	
	for field in $ValueContainer.get_children():
		field.queue_free()
	
	if property_type == "vec3":
		for i in range(3):
			var spin = SpinBox.new()
			spin.set_min(-LIMITS)
			spin.set_max(LIMITS)
			spin.step = 0.01
			
			if value != null:
				spin.value = value[i]
			
			spin.connect("value_changed", self, "on_value_changed")
			$ValueContainer.add_child(spin)
	elif property_type == "list":
		var drop = OptionButton.new()
		for item in value:
			var id = drop.get_item_count()
			drop.add_item(item, id)
		drop.connect("item_selected", self, "on_value_changed")
		$ValueContainer.add_child(drop)

func set_list_selection(text):
	if property_type != "list": return
	
	var drop = $ValueContainer.get_child(0)
	for i in range(drop.get_item_count()):
		if drop.get_item_text(i) == text:
			drop.select(i)
			break

func get_value():
	var value
	
	if property_type == "vec3":
		value = Vector3()
		for i in range(3):
			value[i] = $ValueContainer.get_child(i).value
	elif property_type == "list":
		var drop = $ValueContainer.get_child(0)
		var id = drop.get_selected_id()
		value = drop.get_item_text(id)
	
	return value

func on_value_changed(param):
	emit_signal("property_changed", property_name, get_value())
