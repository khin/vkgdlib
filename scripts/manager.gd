extends Node

################################################################################

signal vert_shader_added(shader_name)
signal frag_shader_added(shader_name)
signal material_added(material_name)

################################################################################

var shaders   = Dictionary() setget , get_shaders
var materials = Dictionary() setget , get_materials
var objects   = Dictionary()

################################################################################

export(NodePath) var _vklib = @"../VKLib"
export(NodePath) var _shader_list
export(NodePath) var _material_list
export(NodePath) var _material_edit_vert_list
export(NodePath) var _material_edit_frag_list

onready var vklib         = get_node(_vklib)
onready var shader_list   = get_node(_shader_list)
onready var material_list = get_node(_material_list)
onready var material_edit_vert_list = get_node(_material_edit_vert_list)
onready var material_edit_frag_list = get_node(_material_edit_frag_list)

var ShaderClass   = preload("res://scripts/shader.gdns")
var MaterialClass = preload("res://scripts/material.gdns")
var GeometryClass = preload("res://scripts/geometry.gdns")

export(PrimitiveMesh) var default_mesh
func _ready():
	if not add_shader("Default vertex",   "res://shaders/default.vert"): return
	if not add_shader("Default fragment", "res://shaders/default.frag"): return
	add_material("Default material")
	
	shader_list.select(0)
	material_list.select(0)
	
	on_material_selected(0)
	update_uniforms()

func _exit_tree():
	vklib.clear_command_buffer()
	
	for object in objects.values():
		object.cleanup()
	
	for material in materials.values():
		material.cleanup()
	
	for shader in shaders.values():
		shader.destroy_module()

################################################################################

enum ShaderType {
	Undefined = -1,
	Vertex    = 0,
	Fragment  = 1
}

func get_shaders(type = ShaderType.Undefined):
	if type == ShaderType.Undefined:
		return shaders
	
	var _shaders = {}
	for _name in shaders.keys():
		if shaders[_name].get_type() == type:
			_shaders[_name] = shaders[_name]
	return _shaders

func get_materials(): return materials

################################################################################

func add_shader(shader_name, filepath):
	if shader_name in shaders.keys():
		return false
	
	var shader = ShaderClass.new()
	shader.set_name(shader_name)
	shader.load(ProjectSettings.globalize_path(filepath))
	
	if not shader.compile():
		printerr("Failed to compile shader '" + filepath + "': " + shader.get_compilation_status_string())
		return false
	
	var material_edit_shader_list = null
	var signal_to_emit = null
	if filepath.ends_with(".vert"):
		material_edit_shader_list = material_edit_vert_list
		signal_to_emit = "vert_shader_added"
	elif filepath.ends_with(".frag"):
		material_edit_shader_list = material_edit_frag_list
		signal_to_emit = "frag_shader_added"
	
	if material_edit_shader_list == null:
		return false
	
	var id = material_edit_shader_list.get_item_count()
	material_edit_shader_list.add_item(shader_name, id)
	material_edit_shader_list.set_item_metadata(id, shader_name)
	
	shader.create_module()
	shaders[shader_name] = shader
	shader_list.add_item(shader_name)
	
	return true

func reload_shader(shader_name):
	if not shader_name in shaders.keys():
		return false
	
	var shader = shaders[shader_name]
	shader.reload()
	
	if not shader.compile():
		printerr("Failed to recompile shader '" + shader_name + "': " + shader.get_compilation_status_string())
		return false
	
	shader.create_module()
	reload_material_shader(shader_name)
	return true

func reload_material_shader(shader_name):
	for material in materials.values():
		if material.get_shader_name(0) == shader_name or material.get_shader_name(1) == shader_name:
			material.set_shader(shaders[shader_name])

func reload_selected_shader():
	var shader_name = shader_list.get_item_text(shader_list.get_selected_items()[0])
	if not shader_name in shaders.keys():
		printerr("No shader selected?")
		return
	
	reload_shader(shader_name)

export(Texture) var default_texture
func add_material(material_name):
	if material_name in materials.keys():
		return false
	
	var material = MaterialClass.new()
	material.set_name(material_name)
	materials[material_name] = material
	material_list.add_item(material_name)
	
	set_material_shader(material_name, "Default vertex")
	set_material_shader(material_name, "Default fragment")
	
	var image = default_texture.get_data()
	material.set_default_texture_image(image)
	material.setup(material_name)
	
	emit_signal("material_added", material_name)
	return true

func get_selected_material_name():
	var selected = material_list.get_selected_items()
	
	if selected.size() == 0:
		return null
	
	return material_list.get_item_text(selected[0])

func get_selected_material():
	return materials[get_selected_material_name()]

################################################################################

func set_material_shader(material_name, shader_name):
	if not material_name in materials.keys(): return
	if not shader_name in shaders.keys(): return
	
	var shader = shaders[shader_name]
	var material = materials[material_name]
	
	material.set_shader(shader)

func material_setup(material_name):
	if not material_name in materials.keys(): return
	var material = materials[material_name]
	material.set_default_texture_image(default_texture.get_data())
	material.setup(material_name)
	
	for geometry in objects.values():
		if geometry.get_material_name() == material.get_name():
			geometry.set_material(material)
	
	on_material_selected(material_list.get_selected_items()[0])
	
	vklib.clear_command_buffer()
	for geometry in objects.values():
		geometry.create_descriptor_set()
	vklib.create_command_buffer()

func add_geometry(geometry_name, mesh):
	if objects.has(geometry_name):
		printerr("There already is a geometry called '", geometry_name, "'. Denied.")
		return false
	
	var geometry = GeometryClass.new()
	geometry.set_array_mesh(mesh)
	objects[geometry_name] = geometry
	vklib.add_geometry(geometry)
	set_geometry_material(geometry_name, "Default material")
	return true

func set_geometry_material(geometry_name, material_name):
	if not material_name in materials.keys(): return
	if not geometry_name in objects.keys(): return
	
	vklib.clear_command_buffer()
	
	objects[geometry_name].set_material(materials[material_name])
	objects[geometry_name].create_descriptor_set()
	
	vklib.create_command_buffer()

################################################################################

export(NodePath) var _uniform_edit
export(PackedScene) var edit_scene
func on_material_selected(id):
	var material_name = material_list.get_item_text(id)
	var mat = materials[material_name]#get_selected_material()
	var uniforms = mat.get_exported_uniforms()
	
	var edit = get_node(_uniform_edit)
	for child in edit.get_children():
		child.queue_free()
	
	for uniform in uniforms:
		var scene = edit_scene.instance()
		edit.add_child(scene)
		scene.set_uniform(uniform)

func _process(delta):
	update_uniforms()

func update_uniforms():
	var edit = get_node(_uniform_edit)
	var uniforms = []
	
	for child in edit.get_children():
		uniforms.push_back(child.get_uniform())
	
	var mat = get_selected_material()
	mat.set_exported_uniforms_values(uniforms)

func on_texture_selected(path):
	var texLoader = get_node("../LoadTextureFile")
	if not texLoader.has_meta("sampler"):
		return
	
	var sampler = texLoader.get_meta("sampler")
	if not sampler.has("name") or not sampler.has("value"):
		return
	
	vklib.clear_command_buffer()
	
	get_selected_material().set_texture(sampler["name"], sampler["value"])
	
	for geometry in objects.values():
		geometry.create_descriptor_set()
	
	vklib.create_command_buffer()
	