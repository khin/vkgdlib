extends Button

export(NodePath) var manager
export(NodePath) var vertex_shader
export(NodePath) var fragment_shader

onready var mgmt = get_node(manager)
onready var vert = get_node(vertex_shader)
onready var frag = get_node(fragment_shader)

func _pressed():
	var material_name = mgmt.get_selected_material_name()
	if material_name == null: return
	
	var vert_name = vert.get_selected_metadata()
	var frag_name = frag.get_selected_metadata()
	
	if vert_name == null: vert_name = "Default vertex"
	if frag_name == null: frag_name = "Default fragment"
	
	mgmt.set_material_shader(material_name, vert_name)
	mgmt.set_material_shader(material_name, frag_name)
	mgmt.material_setup(material_name)


func _on_material_selected(index):
	var current_material = mgmt.get_selected_material()
	
	option_button_select_string(vert, current_material.get_shader_name(0))
	option_button_select_string(frag, current_material.get_shader_name(1))

func option_button_select_string(button, string):
	for i in range(button.get_item_count()):
		if button.get_item_text(i) == string:
			button.select(i)
			break
