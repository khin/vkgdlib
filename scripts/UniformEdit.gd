extends HBoxContainer

export(float) var min_value = -1000
export(float) var max_value = 1000

var manager
var uniform

func set_uniform(uniform):
	if not "name" in uniform.keys() or not "type" in uniform.keys(): return
	
	self.uniform = uniform
	
	$LabelUniformName.text = uniform.name# + " (" + uniform.type + ")"
	
	if uniform.type == "int":
		if not uniform.has("value"): uniform["value"] = 1
		
		var field = SpinBox.new()
		field.min_value = min_value
		field.max_value = max_value
		field.value = uniform.value
		field.step = 1
		field.rounded = true
		add_child(field)
		
	elif uniform.type == "uint":
		if not uniform.has("value"): uniform["value"] = 1
		
		var field = SpinBox.new()
		field.min_value = 0
		field.max_value = max_value
		field.value = uniform.value
		field.step = 1
		field.rounded = true
		add_child(field)
		
	elif uniform.type == "float":
		if not uniform.has("value"): uniform["value"] = 1.0
		
		var field = SpinBox.new()
		field.min_value = min_value
		field.max_value = max_value
		field.value = uniform.value
		field.step = 0.01
		field.rounded = false
		add_child(field)
		
	elif uniform.type == "ivec2":
		if not uniform.has("value"): uniform["value"] = Vector2(1.0, 1.0)
		
		for i in range(2):
			var field = SpinBox.new()
			field.min_value = min_value
			field.max_value = max_value
			field.value = uniform.value[i]
			field.step = 1
			field.rounded = true
			add_child(field)
		
	elif uniform.type == "ivec3":
		if not uniform.has("value"): uniform["value"] = Vector3(1.0, 1.0, 1.0)
		
		for i in range(3):
			var field = SpinBox.new()
			field.min_value = min_value
			field.max_value = max_value
			field.value = uniform.value[i]
			field.step = 1
			field.rounded = true
			add_child(field)
		
	elif uniform.type == "uvec2":
		if not uniform.has("value"): uniform["value"] = Vector2(1.0, 1.0)
		
		for i in range(2):
			var field = SpinBox.new()
			field.min_value = 0
			field.max_value = max_value
			field.value = uniform.value[i]
			field.step = 1
			field.rounded = true
			add_child(field)
		
	elif uniform.type == "uvec3":
		if not uniform.has("value"): uniform["value"] = Vector3(1.0, 1.0, 1.0)
		
		for i in range(3):
			var field = SpinBox.new()
			field.min_value = 0
			field.max_value = max_value
			field.value = uniform.value[i]
			field.step = 1
			field.rounded = true
			add_child(field)
		
	elif uniform.type == "vec2":
		if not uniform.has("value"): uniform["value"] = Vector2(1.0, 1.0)
		
		for i in range(2):
			var field = SpinBox.new()
			field.min_value = min_value
			field.max_value = max_value
			field.value = uniform.value[i]
			field.step = 0.01
			field.rounded = false
			add_child(field)
		
	elif uniform.type == "vec3":
		if not uniform.has("value"): uniform["value"] = Vector3(1.0, 1.0, 1.0)
		
		for i in range(3):
			var field = SpinBox.new()
			field.min_value = min_value
			field.max_value = max_value
			field.value = uniform.value[i]
			field.step = 0.01
			field.rounded = false
			add_child(field)
		
	elif uniform.type == "color":
		if not uniform.has("value"): uniform["value"] = Color(1.0, 1.0, 1.0, 1.0)
		
		var field = ColorPickerButton.new()
		#field.size_flags_horizontal = Control.SIZE_EXPAND_FILL
		field.rect_min_size = Vector2(64, 10)
		field.rect_position = Vector2(64, 10)
		field.color = uniform.value
		add_child(field)
		
	elif uniform.type == "sampler2D":
		var loadControl = get_tree().get_root().get_node("ControlWindow/LoadTextureFile")
		var thumbnail = TextureRect.new()
		if not uniform.has("path"): uniform["path"] = "res://white.jpg"
		#thumbnail.texture = uniform["value"] if uniform.has("value") else load(uniform["path"])
		thumbnail.stretch_mode = TextureRect.STRETCH_SCALE_ON_EXPAND
		thumbnail.rect_size = Vector2(64, 64)
		thumbnail.rect_min_size = Vector2(64, 64)
		thumbnail.expand = true
		add_child(thumbnail)
		
		var button = Button.new()
		button.icon = preload("res://icons/load.svg")
		button.connect("pressed", self, "on_load_texture_press", [loadControl, button, thumbnail])
		add_child(button)
		
		texture_selected(uniform["path"], loadControl, button, thumbnail)
		loadControl.emit_signal("file_selected", uniform["path"])

func on_load_texture_press(loadControl, button, thumbnail):
	loadControl.connect("file_selected", self, "texture_selected", [loadControl, button, thumbnail], CONNECT_ONESHOT)
	loadControl.popup_centered()

func texture_selected(path, loadControl, button, thumbnail):
	var texture = load(path)
	thumbnail.texture = texture
	thumbnail.hint_tooltip = path
	thumbnail.rect_size = Vector2(64, 64)
	thumbnail.rect_min_size = Vector2(64, 64)
	button.hint_tooltip = path
	uniform.path = path
	
	var image = Image.new()
	image.load(uniform.path)
	uniform.value = image
	
	loadControl.set_meta("sampler", uniform)
	manager.on_texture_selected(path)

func get_uniform():
	var value
	
	if uniform.type == "int" or uniform.type == "uint" or uniform.type == "float":
		value = get_child(1).value
	elif uniform.type == "ivec2" or uniform.type == "uvec2" or uniform.type == "vec2":
		value = Vector2(get_child(1).value, get_child(2).value)
	elif uniform.type == "ivec3" or uniform.type == "uvec3" or uniform.type == "vec3":
		value = Vector3(get_child(1).value, get_child(2).value, get_child(3).value)
	elif uniform.type == "vec4" or uniform.type == "color":
		value = get_child(1).color
	
	uniform["value"] = value
	return uniform