extends WindowDialog

export(NodePath) var _manager = @"../Manager"
onready var manager = get_node(_manager)

func on_file_selected(filepath):
	$SourceAddress.text = filepath
	$SourceAddress.hint_tooltip = filepath
	
	if $LineEditName.text.empty():
		$LineEditName.text = filepath.get_file()

func on_create_shader():
	if $LineEditName.text.empty(): return
	
	if manager.add_shader($LineEditName.text, $SourceAddress.text):
		$LineEditName.text = String()
		$SourceAddress.text = String()
		hide()
