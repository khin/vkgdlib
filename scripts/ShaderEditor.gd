extends Control

export(NodePath) var _manager
export(NodePath) var _shader_list

onready var manager = get_node(_manager)
onready var shader_list = get_node(_shader_list)

var current_shader
var filepath

func on_shader_activated(index):
	var shader_name = shader_list.get_item_text(index)
	if not manager.shaders.keys().has(shader_name):
		printerr("Ermmm this shouldn't happen")
		return
	
	var shader = manager.shaders[shader_name]
	var filepath = shader.get_filepath()
	load_source(shader_name, filepath)

func load_source(shader_name, file_path):
	current_shader = shader_name
	filepath = file_path
	
	var file = File.new()
	file.open(filepath, File.READ)
	
	if not file.is_open():
		printerr("Failed to open file: " + filepath)
		return
	
	var source = file.get_as_text()
	file.close()
	
	$TextEdit.text = source

func save_file():
	if filepath.empty():
		printerr("Filename not set, can't save")
		return
	
	var file = File.new()
	file.open(filepath, File.WRITE)
	file.store_string($TextEdit.text)
	file.close()
